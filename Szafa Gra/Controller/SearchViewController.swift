//
//  SearchViewController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 29/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import BluetoothKit

class SearchViewController: UIViewController, UICollectionViewDelegate, BKRemotePeerDelegate, BKRemotePeripheralDelegate {
    
    
    private var nearbyDevices: [BKDiscovery] = []
    private var shouldLoad: Bool = true
    private var loading: Bool = false
    private var partyIcons: [RoundShadowView]?
    private var selectedIndex: Int?
    private lazy var waitingView = self.configureWaitingView()
    var partyController :PlaylistViewController!
    
    
    @IBOutlet weak var backgroundView: UIImageView!
    
    @IBOutlet weak var radar: UIView!
    
    @IBOutlet weak var radarWidth: NSLayoutConstraint!
    
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         print("did")
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.applyMask()
       
        self.loadNearbyDevices()
    
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        BluetoothManager.central.interruptScan()
        RootViewController.stopLoading()
        self.partyController.showPartyIfPossible { () -> () in
            guard let party = self.partyController.party else {return}
            if(party.type == .hosted && party.songs!.count > 0){
                self.partyController.updateTableAfterAddingSongs(firstSongs: false)
                self.partyController.playerController.updatePlayerView(withSong: self.partyController.currentSong)
            }
        }
    }
    
    
    
    func connectToParty(atIndex index: Int) {
        RootViewController.startLoading()
        BluetoothManager.central.connect(remotePeripheral: self.nearbyDevices[index].remotePeripheral) { (peripheral, error) in
            if(error == nil){
                peripheral.peripheralDelegate = self
                peripheral.delegate = self
            }
            else{
                DispatchQueue.main.async {
                    RootViewController.stopLoading()
                    self.loadNearbyDevices()
                }
            }
            
            
        }
        
    }
    

    
    
    func loadNearbyDevices() {
        var after = 0.0
        if(self.loading){
            self.blockLoading()
            after = 1.5
        }
        self.nearbyDevices = []
        self.partyIcons?.forEach{
            $0.removeFromSuperview()
        }
        self.partyIcons = nil
        BluetoothManager.central.interruptScan()
        BluetoothManager.disconnectFromAllDevices()
        self.hideWaitingView()
        DispatchQueue.main.asyncAfter(deadline: .now() + after) {
            
            
            self.startLoading()
            
            BluetoothManager.searchForParties(withProgress:{
                devices in
                guard let devices = devices else {return}
                self.showIcons(forDevices:devices)
                
            }){(devices, error) in
                defer{
                    DispatchQueue.main.async {
                    self.stopLoading()
                    }
                }
                guard let devices = devices, error != nil else {return}
                
            }
        }
        
    }
    
    func startLoading(){
        self.shouldLoad = true
        if(self.shouldLoad && !self.loading){
            self.refreshButton.isEnabled = false
            self.loading = true
            self.animateRadar()
        }
    }
    
    func blockLoading(){
        self.refreshButton.isEnabled = false
        self.shouldLoad = false
    }
    
    func animateRadar(){
        
        self.radar.alpha = 1.0
        self.radarWidth.constant = 1000.0
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.radar.alpha = 0.0
            
            self.radar.layer.cornerRadius = 500.0
            self.radar.superview!.layoutIfNeeded()
        }) { (_) in
            self.radarWidth.constant = 2.0
            self.radar.layer.cornerRadius = 1.0
            self.radar.superview!.layoutIfNeeded()
            if(self.shouldLoad){
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.animateRadar()
                })
            }
            else{
                self.loading = false
                self.shouldLoad = true
            }
        }
    }
    
    func stopLoading(){
        self.refreshButton.isEnabled = true
        self.shouldLoad = false
    }
    
    func showIcons(forDevices devices:[BKDiscovery]){
        self.nearbyDevices.append(contentsOf: devices)
        var usedCoordinates: [CGPoint] = []
        if(self.partyIcons == nil){
            self.partyIcons = []
        }
        self.partyIcons?.forEach{
            usedCoordinates.append($0.center)
        }
        let iconHeight: CGFloat = 40.0
        let iconWidth: CGFloat = 150.0
        var x: CGFloat = -10000.0
        var y: CGFloat = -10000.0
        for (ix, discovery) in devices.enumerated(){
            repeat{
                x = CGFloat.random(in: iconHeight+15.0...self.view.frame.size.width-iconWidth-15.0)
                y = CGFloat.random(in: iconHeight+65.0...self.view.frame.size.height-iconHeight-15.0)
            } while(usedCoordinates.filter{
                abs(x-$0.x) < iconWidth && abs(y-$0.y) < iconHeight
            }.count > 0)
            
            let point = CGPoint(x: x,y: y)
            usedCoordinates.append(point)
            
            let icon = UIButton(frame: CGRect(x: 0, y: 0, width: iconHeight, height: iconHeight))
            icon.layer.masksToBounds = true
            icon.layer.cornerRadius = iconHeight*0.5
            icon.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            icon.setImage(UIImage(named: "songPlaceholder"), for: .normal)
            icon.tag = ix
            icon.addTarget(self, action: #selector(partyClicked(_:)), for: .touchUpInside)
            
            let nameButton = UIButton(frame: CGRect(x: iconHeight+2, y: 0, width: iconWidth-iconHeight-10, height: iconHeight))
            nameButton.layer.masksToBounds = true
            nameButton.layer.cornerRadius = iconHeight*0.5
            nameButton.tintColor = .white
            nameButton.titleLabel?.minimumScaleFactor = 0.75
            nameButton.titleLabel?.lineBreakMode = .byTruncatingTail
            nameButton.titleLabel?.adjustsFontSizeToFitWidth = true
            nameButton.setTitle(discovery.localName, for: .normal)
            nameButton.tag = ix
            nameButton.addTarget(self, action: #selector(partyClicked(_:)), for: .touchUpInside)
            
            
            
            let shadowView = RoundShadowView(frame: CGRect(x: 0, y: 0, width: iconWidth, height: iconHeight), shadowColor: UIColor.myGreen.cgColor, shadowOffset: CGSize(width:0.0, height: 0.0), shadowRadius: 6.0, shadowOpacity: 0.8)
            shadowView.addSubview(icon)
            shadowView.addSubview(nameButton)
            shadowView.center = point
            shadowView.tag = ix
            self.view.addSubview(shadowView)
            self.partyIcons?.append(shadowView)
        }
    
    }
    
    
    func showParty(_ party: Party){
        RootViewController.stopLoading{
            self.partyController.party = party
            self.partyController.tableView.reloadData()
            BluetoothManager.central.delegate = self.partyController.bluetoothDelegate
            self.navigationController?.popViewController(animated: true)
            self.partyController.playerController.selectSong(atIndex: party.songIndex)
            RootViewController.shared?.stateButton?.isSelected = true
            
        }
    }
    
    func showWaitingView(forDevice device: BKRemotePeer){
        var index: Int?
        self.partyIcons?.forEach{
            $0.isUserInteractionEnabled = true
        }
        for (ix, discovery) in self.nearbyDevices.enumerated(){
            if(discovery.remotePeripheral.identifier.uuidString == device.identifier.uuidString){
                index = ix
                
                break
            }
        }
        if(index != nil){
            self.waitingView.show(attachedToView: self.partyIcons![index!], inSuperView: self.view)
            }
    }
    
    
    @objc func closeWaitingViewClicked(){
        DispatchQueue.main.async{
            self.loadNearbyDevices()
        }
        
        
    }
    
    func hideWaitingView(){
        self.partyIcons?.forEach{
            $0.isUserInteractionEnabled = true
        }
        self.waitingView.hide()
    }
    
    func configureWaitingView() -> PinnedView {
        
        let view = PinnedView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        view.verticalPadding = 0.0
        view.verticalSpacing = 15.0
        let blur = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        blur.frame = view.frame
        view.addSubview(blur)
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 20
        let closeButton = UIButton(frame: CGRect(x: 0,y: 0,width: 40,height: 40))
        closeButton.imageEdgeInsets = UIEdgeInsets(top: 3.0, left: 3.0, bottom: 3.0, right: 3.0)
        closeButton.setImage(UIImage(named:"close"), for: .normal)
        closeButton.addTarget(self, action: #selector(closeWaitingViewClicked), for: .touchUpInside)
        closeButton.tintColor = .myGreen
        closeButton.backgroundColor = UIColor(white: 0.0, alpha: 0.25)
        let label = UILabel(frame: CGRect(x: 40, y: 0, width: 110, height: 40))
        label.textColor = .lightGray
        label.textAlignment = .center
        label.text = "Waiting..."
        label.backgroundColor = UIColor(white: 0.0, alpha: 0.1)
        blur.contentView.addSubview(label)
        blur.contentView.addSubview(closeButton)
        view.alpha = 0.0
        return view
    }
    
    @IBAction func refreshClicked(_ sender: Any) {
        self.loadNearbyDevices()
    }
    @objc func partyClicked(_ button: UIButton){
        
        self.connectToParty(atIndex: button.tag)
    }
    
    func remotePeer(_ remotePeer: BKRemotePeer, didSendArbitraryData data: Data) {
        guard let party = partyFromData(data) else {
            if let dict = dictFromData(data), let result = dict["result"] as? String, result == "ready" {
                BluetoothManager.sendConnectionRequest(toHost: remotePeer)
                self.showWaitingView(forDevice: remotePeer)
            }
            else{
                
                self.presentAlert(title: "Access denied", message: "Host of this party denied you the access.") {
                    DispatchQueue.main.async{
                        self.loadNearbyDevices()
                    }
                }
                
            }
            
            RootViewController.stopLoading()
            return
        }
        self.hideWaitingView()
        remotePeer.delegate = self.partyController.bluetoothDelegate
        self.showParty(party)
    }
    
    func remotePeripheral(_ remotePeripheral: BKRemotePeripheral, didUpdateName name: String) {
        
    }
    
    func remotePeripheralIsReady(_ remotePeripheral: BKRemotePeripheral) {
        
    }
    
    
}
