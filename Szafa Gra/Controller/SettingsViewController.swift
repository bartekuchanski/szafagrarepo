//
//  SettingsViewController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 29/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, DataViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var backgroundView: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var profileButton: UIButton!
    
    var pageIndex = 0
    static var shared: SettingsViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        SettingsViewController.shared = self
        self.nameField.delegate = self
        self.nameField.attributedPlaceholder = NSAttributedString(string:"Your nickname", attributes: [NSAttributedString.Key.font : UIFont(name: "HelveticaNeue", size: 20) as Any, .foregroundColor : UIColor.lightGray] )
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupView()
        
    }
    
    
    @IBAction func profileClicked(_ sender: Any) {
        self.presentPictureSources()
    }
    
    func setupView(){
        self.profileButton.layer.masksToBounds = true
        self.profileButton.layer.cornerRadius = self.profileButton.frame.size.width*0.5
        self.profileButton.layer.borderColor = UIColor.white.cgColor
        self.profileButton.layer.borderWidth = 1.0
        
        let user = User.current
        
        self.profileButton.setImage(User.current?.profileImage, for: .normal)
        self.backgroundView.image = User.current?.profileImage
        if(User.current != nil && User.current?.nickname != "Anonymous"){
            self.nameField.text = User.current!.nickname
        }
    }
    
    func presentPictureSources(){
        let title = "Choose source"
        let message = "Choose source of your profile picture"
        let alert = UIAlertController(title: "" , message: "", preferredStyle: .actionSheet)
        alert.setValue(NSAttributedString(string: title, attributes: [.font : UIFont(name: "HelveticaNeue-Bold", size: 16) as Any, .foregroundColor : UIColor.white as Any]), forKey: "attributedTitle")
        alert.setValue(NSAttributedString(string: message, attributes: [.font : UIFont(name: "HelveticaNeue", size: 15) as Any, .foregroundColor : UIColor.lightGray as Any]), forKey: "attributedMessage")
        alert.view.tintColor = .myGreen
        alert.addAction(UIAlertAction(title: "Photo album", style: .default, handler: { (_) in
            self.presentPicker(ofType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.presentPicker(ofType: .camera)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in
            
        }))
        
        self.present(alert, animated: true)
        
    }
    
    func presentPicker(ofType type: UIImagePickerController.SourceType) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.mediaTypes = ["public.image"]
        pickerController.sourceType = type
        self.present(pickerController, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
        User.current?.profileImage = image
        self.profileButton.setImage(image, for: .normal)
        self.backgroundView.image = image
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backgroundClicked(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    // MARK: UITextFieldDelegate methods:
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        User.current?.nickname = textField.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
