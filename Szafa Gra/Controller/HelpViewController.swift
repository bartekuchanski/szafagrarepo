//
//  HelpViewController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 29/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController, DataViewController {
    static var shared: HelpViewController?
    
    var pageIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        HelpViewController.shared = self
        // Do any additional setup after loading the view.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
