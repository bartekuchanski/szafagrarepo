//
//  RootViewController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 28/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

class RootViewController: UIViewController, UIPageViewControllerDelegate {

    var pageViewController: UIPageViewController?
    var currentIndex : Int = 1
    var nextIndex : Int = 1
    
    var controlButtons: [UIButton]?
    var stateButton: UIButton?
    var dotView : UIView?
    var controlsShown : Bool = false
    
    let dotColors = [UIColor.myGreen, UIColor.myGreen, UIColor(white: 0.2, alpha: 1)]
    
    
    static var shared: RootViewController?
    private let dotWidth = 6.0
    private let gap = 40.0
    private static var shouldLoad = false
    public static var loadingView : UIView?
    
    public static func startLoading(){
        if(self.loadingView == nil)
        {
            self.createLoadingView()
        }
        self.shouldLoad = true
        self.animateLoadingView()
    }
    
    public static func stopLoading(_ completion: (()->())?)
    {
        
        self.shouldLoad = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
             UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                self.loadingView?.alpha = 0.0
            }, completion: {(completed) in
               let subviews = self.loadingView?.subviews[0].subviews
                subviews?.forEach{
                    $0.backgroundColor = UIColor.init(white:0.2, alpha: 1)
                }
                completion?()
            })
        })
        
    }
    
    public static func stopLoading(){
        self.shouldLoad = false
        UIView.animate(withDuration: 0.3) {
            self.loadingView?.alpha = 0.0
        }
        let subviews = self.loadingView?.subviews
        subviews?.forEach{
            $0.backgroundColor = UIColor.init(white:0.2, alpha: 1)
            $0.layer.removeAllAnimations()
        }
        self.loadingView?.layer.removeAllAnimations()
        
        
        
    }
    
    private static func animateLoadingView(){
        if(self.shouldLoad){
            UIView.animate(withDuration: 0.3) {
                self.loadingView?.alpha = 1.0
            }
            let subviews = self.loadingView?.subviews[0].subviews
            var count = 0
            var iteration = 0
            subviews?.forEach{
                let view = $0
                UIView.animate(withDuration: 0.6, delay: 0.3*Double(iteration+1), options: UIView.AnimationOptions.curveEaseInOut, animations: {
                    view.backgroundColor = view.backgroundColor == .myGreen ?  UIColor.init(white:0.5, alpha: 1) : .myGreen
                }, completion: { (completed) in
                    count+=1
                    if(count == 2)
                    {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                        
                            self.animateLoadingView()
                       
                        })
                    }
                })
                iteration += 1
                
            }
        }
    }
    private static func createLoadingView(){
        guard let keyWindow = UIApplication.shared.keyWindow else {return}
        
        let overlay = UIView(frame:keyWindow.frame)
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        overlay.alpha = 0.0
        let container = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        container.layer.masksToBounds = true
        container.layer.cornerRadius = 15
        container.backgroundColor = UIColor(white: 0.1, alpha: 1.0)
        
        for i in 0...2
        {
            let circle = UIView(frame: CGRect(x: 10, y: 10+(i*30), width: 80, height: 20))
            circle.backgroundColor = UIColor.init(white:0.2, alpha: 1)
            circle.layer.masksToBounds = true
            circle.layer.cornerRadius = 7.5
            container.addSubview(circle)
            
        }
        overlay.addSubview(container)
        keyWindow.addSubview(overlay)
        container.center = overlay.center
        self.loadingView = overlay
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .dark)
        UILabel.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).textColor = .white
        
        RootViewController.shared = self
        // Do any additional setup after loading the view.
        // Configure the page view controller and add it as a child view controller.
        self.configureNavigationBar()
        self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.pageViewController!.delegate = self

        let startingViewController: UIViewController = self.modelController.viewControllerAtIndex(1, storyboard: self.storyboard!)! as! UIViewController
        let viewControllers = [startingViewController]
        self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: false, completion: {done in })

        self.pageViewController!.dataSource = self.modelController

        self.addChild(self.pageViewController!)
        self.view.addSubview(self.pageViewController!.view)

        // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
        var pageViewRect = self.view.bounds
        if UIDevice.current.userInterfaceIdiom == .pad {
            pageViewRect = pageViewRect.insetBy(dx: 40.0, dy: 40.0)
        }
        self.pageViewController!.view.frame = pageViewRect

        self.pageViewController!.didMove(toParent: self)
       
        
    }

    override func viewDidAppear(_ animated: Bool) {
        if(self.dotView == nil){ self.configureDotsIndicator(self.modelController.pageData.count, 1)
            self.configureControls()
        }
    }
    
    
    var modelController: ModelController {
        // Return the model controller object, creating it if necessary.
        // In more complex implementations, the model controller may be passed to the view controller.
        if _modelController == nil {
            _modelController = ModelController()
        }
        return _modelController!
    }

    var _modelController: ModelController? = nil

    
    // MARK: - UIPageViewController delegate methods

    func pageViewController(_ pageViewController: UIPageViewController, spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewController.SpineLocation {
        if (orientation == .portrait) || (orientation == .portraitUpsideDown) || (UIDevice.current.userInterfaceIdiom == .phone) {
            // In portrait orientation or on iPhone: Set the spine position to "min" and the page view controller's view controllers array to contain just one view controller. Setting the spine position to 'UIPageViewController.SpineLocation.mid' in landscape orientation sets the doubleSided property to true, so set it to false here.
            let currentViewController = self.pageViewController!.viewControllers![1]
            let viewControllers = [currentViewController]
            self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: {done in })

            self.pageViewController!.isDoubleSided = false
            return .min
        }

        // In landscape orientation: Set set the spine location to "mid" and the page view controller's view controllers array to contain two view controllers. If the current page is even, set it to contain the current and next view controllers; if it is odd, set the array to contain the previous and current view controllers.
        let currentViewController = self.pageViewController!.viewControllers![1] as! DataViewController
        var viewControllers: [UIViewController]

        let indexOfCurrentViewController = self.modelController.indexOfViewController(currentViewController)
        if (indexOfCurrentViewController == 0) || (indexOfCurrentViewController % 2 == 0) {
            let nextViewController = self.modelController.pageViewController(self.pageViewController!, viewControllerAfter: currentViewController as! UIViewController)
            viewControllers = [currentViewController, nextViewController!] as! [UIViewController]
        } else {
            let previousViewController = self.modelController.pageViewController(self.pageViewController!, viewControllerBefore: currentViewController as! UIViewController)
            viewControllers = [previousViewController!, currentViewController] as! [UIViewController]
        }
        self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: {done in })
        
        return .mid
    }

    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        self.nextIndex = self.modelController.indexOfViewController(pendingViewControllers.first as! DataViewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed){
            self.currentIndex = self.nextIndex
        }
        else{
            self.nextIndex = self.currentIndex
        }
        
        self.highlight(currentIndex: self.currentIndex)
        
    }

    
    // MARK: - RootViewController methods:
    
    func configureNavigationBar(){
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    self.navigationController?.navigationBar.shadowImage = UIImage()
    self.navigationController?.navigationBar.isTranslucent = true
    self.navigationController?.view.backgroundColor = .clear
        
    }
    
    
    func configureDotsIndicator(_ dotCount: Int, _ initialIndex: Int){
        
        let dotView = UIView(frame: self.calculateRectForDotView(dotCount))
        
        for i in stride(from: 0.0, to: Double(dotCount), by: 1.0)
        {
            
            let dot = RoundShadowView(frame: CGRect(x: i*dotWidth + (i+1.0)*gap , y: dotWidth, width: dotWidth, height: dotWidth), shadowColor: UIColor.black.cgColor, shadowOffset: CGSize(width: 1.0, height: 1.0), shadowRadius: 2.0, shadowOpacity: 0.8)
            dot.tag = Int(i)
            
            dotView.addSubview(dot)
        }
        
        self.view.addSubview(dotView)
        self.dotView = dotView
        self.highlight(currentIndex: initialIndex)
    }

    func configureControls(){
        
        let backward = UIButton(frame: CGRect(x: 0, y: 0, width: 2, height: 2))
        let add = UIButton(frame:backward.frame)
        let more = UIButton(frame:backward.frame)
        let forward = UIButton(frame:backward.frame)
        let play = UIButton(frame:backward.frame)
        let state = StateButton(frame: CGRect(x: 0, y: 0, width: 80, height: 2), normalColor: .red, selectedColor: .myGreen, normalTitle: "offline", selectedTitle: "online")
        state.alpha = 0
        add.setImage(UIImage(named: "add"), for: .normal)
        add.setImage(UIImage(named: "addFilled"), for: [.normal, .highlighted])
        
        more.setImage(UIImage(named: "more"), for: .normal)
        more.setImage(UIImage(named: "moreFilled"), for: [.normal, .highlighted])
        
        backward.setImage(UIImage(named: "backwardFilled"), for: .normal)
        backward.setImage(UIImage(named: "backward"), for: [.normal, .highlighted])
        
        forward.setImage(UIImage(named: "forwardFilled"), for: .normal)
        forward.setImage(UIImage(named: "forward"), for: [.normal, .highlighted])
        
        play.setImage(UIImage(named: "playFilled"), for: .normal)
        play.setImage(UIImage(named: "play"), for: [.normal, .highlighted])
        play.setImage(UIImage(named: "pauseFilled"), for: .selected)
        play.setImage(UIImage(named: "pause"), for: [.highlighted, .selected])
        
        let subviews = [more, backward, play, forward, add]
        subviews.forEach{
            $0.alpha = 0
            $0.tintColor = .myGreen
            self.view.addSubview($0)
        }
        self.view.addSubview(state)
        play.center = self.dotView!.center
        state.center = play.center
        backward.center = CGPoint(x: play.center.x - CGFloat(1.5*self.gap), y: play.center.y)
        more.center = CGPoint(x: play.center.x - CGFloat(3.0*self.gap), y: play.center.y)
        more.imageEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        forward.center = CGPoint(x: play.center.x + CGFloat(1.50*self.gap), y: play.center.y)
        add.center = CGPoint(x: play.center.x + CGFloat(3.0*self.gap), y: play.center.y)
        add.imageEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        self.controlButtons = subviews
        self.stateButton = state
    }
    
    func calculateRectForDotView(_ dotCount: Int) -> CGRect {
        
        let width = (Double(dotCount) * dotWidth) + ((Double(dotCount) + 1.0) * gap)
        let height = dotWidth*3.0
        let originX = 0.5*(Double(UIApplication.shared.keyWindow!.frame.width) - width)
        let originY = Double(UIApplication.shared.keyWindow!.frame.height - 100.0)
        
        return CGRect(origin: CGPoint(x: originX, y: originY), size: CGSize(width: width, height: height))
    }
    
    func highlight(currentIndex: Int){
        
        let subviews = self.dotView?.subviews
        subviews?.forEach{
            let dot = $0 as? RoundShadowView
            dot?.shadowLayer?.fillColor = dotColors[currentIndex].cgColor
            
            $0.layer.opacity = $0.tag == currentIndex ? 1.0 : 0.4
        }
       
        let statusBarStyle : UIStatusBarStyle = dotColors[currentIndex] == UIColor.myGreen ? .lightContent : .default
        self.pageViewController?.viewControllers?.first?.setStatusBarStyle(statusBarStyle)
    }
    
    
    func showHideControls(show: Bool){
        
        let shouldShowMusicControls = (PlaylistViewController.shared.party?.type == .hosted || PlaylistViewController.shared.party?.type == .saved)
        
        let controlButtons = show && !shouldShowMusicControls ? [self.controlButtons!.first!, self.controlButtons!.last!] : self.controlButtons!
        if(show){
            self.dotView?.alpha = 0.0
        }
        UIView .animate(withDuration: 0.2, delay: 0.0, options:.curveEaseIn, animations: {
            controlButtons.forEach{
                let center = $0.center
                $0.frame = CGRect(origin: $0.frame.origin, size: CGSize(width: show ? 40 : 2, height: show ? 40 : 2))
                $0.center = center
                $0.alpha = show ? 1.0 : 0.0
            }
            self.dotView?.alpha = show ? 0.0 : 1.0
            
            
            if(!shouldShowMusicControls){
                let state = self.stateButton!
                let center = state.center
                
                state.frame = CGRect(origin: state.frame.origin, size: CGSize(width: state.frame.size.width, height: show ? 30 : 2))
                state.center = center
                state.layer.cornerRadius = show ? 10 : 1
                
                state.alpha = show ? 1.0 : 0.0
            }
            
        }, completion: {completed in
            self.controlsShown = show
            
        })
    }
    
}

class StateButton: UIButton{
    var normalColor = UIColor.white
    var selectedColor = UIColor.white
    
    init(frame: CGRect, normalColor: UIColor, selectedColor: UIColor, normalTitle: String, selectedTitle: String){
        super.init(frame: frame)
        self.normalColor = normalColor
        self.selectedColor = selectedColor
        self.setTitle(normalTitle, for: .normal)
        self.setTitle(selectedTitle, for: .selected)
        self.backgroundColor = normalColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? self.selectedColor : self.normalColor
        }
    }
    
}

class RoundShadowView: UIView {
    
    fileprivate var shadowLayer: CAShapeLayer!
    var shadowColor: CGColor = UIColor.black.cgColor
    var shadowOffset: CGSize = CGSize(width: 2,height: 2)
    var shadowRadius: CGFloat = 5.0
    var shadowOpacity: Float = 0.6
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init(frame:CGRect, shadowColor: CGColor, shadowOffset: CGSize, shadowRadius: CGFloat, shadowOpacity: Float){
        
        self.init()
        
        self.shadowOffset = shadowOffset
        self.shadowRadius = shadowRadius
        self.shadowColor = shadowColor
        self.shadowOpacity = shadowOpacity
        self.frame = frame
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            let cornerRadius = self.frame.size.width*0.5
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = UIColor.myGreen.cgColor
            
            shadowLayer.shadowColor = shadowColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = shadowOffset
            shadowLayer.shadowOpacity = shadowOpacity
            shadowLayer.shadowRadius = shadowRadius
            
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    
}





