//
//  BluetoothManager.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 04/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import BluetoothKit

class BluetoothManager: BKAvailabilityObserver, BKCentralDelegate {
    static var shared: BluetoothManager = BluetoothManager()
    static var central = BKCentral()
    static var peripheral = BKPeripheral()
    

    static func searchForParties(withProgress progress: @escaping(([BKDiscovery]?) -> ()), completion: @escaping (([BKDiscovery]?, BKError?) -> ())){
        central.addAvailabilityObserver(self.shared)
        do {
            let serviceUUID = UUID(uuidString: "6E6B5C64-FAF7-40AE-9C21-D4933AF45B23")!
            let characteristicUUID = UUID(uuidString: "477A2967-1FAB-4DC5-920A-DEE5DE685A3D")!
            let configuration = BKConfiguration(dataServiceUUID: serviceUUID as UUID, dataServiceCharacteristicUUID: characteristicUUID)
            try central.startWithConfiguration(configuration)
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                central.scanWithDuration(10, progressHandler: { newDiscoveries in
                    print("New discoveries: \(newDiscoveries)")
                    
                    progress(newDiscoveries)
                    
                }, completionHandler: { result, error in
                    
                    completion(result, error)
                    print("Discovery error: \(String(describing: error?.localizedDescription))")
                    print("Discovery result: \(result)")
                    // Handle error.
                    // If no error, handle result, [BKDiscovery].
                })
            }
            
        } catch let error {
            // Handle error.
            print(error.localizedDescription)
        }
    }
    
    static func connectToPeripheral(peripheral: BKRemotePeripheral){
        central.connect(remotePeripheral: peripheral) { (peripheral, error) in
            
        }
    }
    
    
    
    static func advertise(withDelegate delegate: BKPeripheralDelegate, partyName name: String){
        
        peripheral.delegate = delegate
        do {
            let serviceUUID = UUID(uuidString: "6E6B5C64-FAF7-40AE-9C21-D4933AF45B23")!
            let characteristicUUID = UUID(uuidString: "477A2967-1FAB-4DC5-920A-DEE5DE685A3D")!
            let localName = name
            let configuration = BKPeripheralConfiguration(dataServiceUUID: serviceUUID, dataServiceCharacteristicUUID:  characteristicUUID, localName: localName)
        
            try peripheral.startWithConfiguration(configuration)
            // You are now ready for incoming connections
            print("Advertising...")
        } catch let error {
            print("Advertise error: \(error.localizedDescription)")
            // Handle error.
        }
    }
    
    static func updatePartyOnRemoteDevices(_ party: Party){
        var partyToSend = party
        partyToSend.type = .remote
        peripheral.connectedRemoteCentrals.forEach{
            self.sendParty(partyToSend, toDevice: $0)
        }
    }
    
    
    static func sendParty(_ party: Party, toDevice device: BKRemotePeer){
        
        peripheral.sendData(dataFromParty(party)!, toRemotePeer: device) { (data, peer, error) in
            
        }
    }
    
    static func sendSongs(_ songs: [Song]){
        guard let host = central.connectedRemotePeripherals.first else {return}
        let dataArray = songs.map{
            dataFromSong($0)
        }
        
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: dataArray, requiringSecureCoding: false)
        
        
            central.sendData(data, toRemotePeer: host) { (data, peer, error) in
                
            }
        }
        catch _ {
            return
        }
    }
    
    
    static func sendConnectionRequest(toHost host: BKRemotePeer){
        let profileImage = UIImage.resizeImage(image: User.current!.profileImage!, targetSize: CGSize(width: 120.0, height: 120.0))
        let imageData = profileImage.jpegData(compressionQuality: 0.5)
        let imageString = imageData?.base64EncodedString(options: .lineLength64Characters)
        
        let dict : [String : Any] = ["profileImage" : imageString! as Any, "nickname" : User.current!.nickname!]
        do{
            let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            
            central.sendData(data, toRemotePeer: host, completionHandler: { (data, peer, error) in
                
            })
          
        } catch let error {
            
        }
    }
    
    static func denyAccess(forDevice device: BKRemotePeer){
        let dict: [String : String] = ["result" : "denied"]
        peripheral.sendData(dataFromDict(dict)!, toRemotePeer: device) { (_, _, _) in}
    }
  
    static func disconnectFromUser(_ user: User){
        for device in peripheral.connectedRemoteCentrals{
            if(device.identifier.uuidString == user.id){
                self.denyAccess(forDevice: device)
            }
        }
 
    }
    
    static func disconnectFromAllDevices(){
        self.disconnectFromCentrals()
        self.disconnectFromPeripheral()
    }
    
    static func disconnectFromCentrals(){
        do{
            try peripheral.stop()
        }
        catch let error{
            print("Stopping peripheral error: \(error.localizedDescription)")
            
        }
    }
    
    
    static func disconnectFromPeripheral(){
        for peripheral in central.connectedRemotePeripherals{
            do{
                try central.disconnectRemotePeripheral(peripheral)
            }
            catch let error{
                print("Disconnecting peripherals error: \(error.localizedDescription)")
            }
        }
        do{
            try central.stop()
        }
        catch let error{
            print("Stopping central error \(error.localizedDescription)")
        }
    }
    
    
    // MARK: BKAvailabilityObserver methods:
    
    func availabilityObserver(_ availabilityObservable: BKAvailabilityObservable, availabilityDidChange availability: BKAvailability) {
        
    }
    
    func availabilityObserver(_ availabilityObservable: BKAvailabilityObservable, unavailabilityCauseDidChange unavailabilityCause: BKUnavailabilityCause) {
        
    }
    
    // MARK: BKCentralDelegate methods:
    
    func central(_ central: BKCentral, remotePeripheralDidDisconnect remotePeripheral: BKRemotePeripheral) {
        
    }
}

