//
//  CreateViewController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 30/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

class CreateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var savedParties: [Party]?
    
    var partyController : PlaylistViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.nameField.delegate = self
        self.loadSavedParties()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.setStatusBarStyle(.lightContent)
        self.partyController.showPartyIfPossible(completion: {})
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func prepareView(){
        self.setStatusBarStyle(.lightContent)
       self.nameField.attributedPlaceholder = NSAttributedString(string: "Name your party", attributes: [NSAttributedString.Key.font : UIFont(name: "HelveticaNeue", size: 19)!, NSAttributedString.Key.foregroundColor : UIColor.lightGray.withAlphaComponent(0.7)])
    }
    
    @IBAction func acceptClicked(_ sender: Any)
    {
        guard let text = self.nameField.text, text.count > 0 else {self.presentAlert(title: "No name", message: "Please name your party before starting it", nil); return}
        self.createParty()
        self.navigationController?.popViewController(animated: true)
    }
    
    func createParty(){
        if(self.tableView.indexPathForSelectedRow == nil){
        self.partyController.party = Party(name: self.nameField.text, type: .hosted, songs: [], songIndex: 0, connectedUsers: [String : User]())
        }
        else{
            var party = self.savedParties![self.tableView.indexPathForSelectedRow!.row]
            party.connectedUsers = [String : User]()
            party.type = .saved
            self.partyController.party = party
        }
        
        BluetoothManager.advertise(withDelegate:self.partyController.bluetoothDelegate, partyName: self.partyController.party!.name!)
    }
    
    func loadSavedParties(){
        self.savedParties = []
        guard let savedDataDict = UserDefaults.standard.object(forKey: "savedParties") as? [String:Data] else { return}
        
        for (name,data) in savedDataDict{
            guard let party = partyFromData(data) else {continue}
            self.savedParties?.append(party)
        }
        self.tableView.reloadData()
    }
    
    @IBAction func backgroundClicked(_ sender: UITapGestureRecognizer) {
         if(self.nameField.isFirstResponder){
            self.view.endEditing(true)
        }
         else{
            guard sender.location(in: self.view).y < self.tableView.frame.origin.y + self.tableView.frame.size.height else {return}
            guard let indexPath = self.tableView.indexPathForSelectedRow else {return}
            
            self.tableView.deselectRow(at: indexPath , animated: true)
            self.nameField.text = nil
        }
    }
    
    
    // MARK: UITableViewDataSource methods:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedParties?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PartyCell", for: indexPath) as UITableViewCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    
    
    
    // MARK: UITableViewDelegate methods:
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.nameField.text = self.savedParties![indexPath.row].name
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let partyCell = cell as! PartyCell
        partyCell.setupCell(withParty: self.savedParties![indexPath.row], atIndex: indexPath.row)
        
    }

    // MARK: UITextFieldDelegate methods:
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
