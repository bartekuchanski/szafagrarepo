//
//  PlaylistDelegate.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 17/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

class PlaylistDelegate: NSObject, UITableViewDelegate {

    var party: Party? {
        return PlaylistViewController.shared.party
    }
    
    var longPressedIndex: Int? {
        return PlaylistViewController.shared.playlistDataSource.longPressedIndex
    }
    // MARK: UITableViewDelegate methods:
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        guard self.party?.type == .hosted else {return nil}
        
        if(self.party!.songs![indexPath.row].source!.isSpotify && spotifyTracksDisabled){
            spotifyTracksDisabled = false
            spotifyAuthorized = false
        }
        
        if(tableView.indexPathForSelectedRow == nil || indexPath.row != self.party!.songIndex){
            PlaylistViewController.shared.playerController.handleSelection(atIndex: indexPath.row)
        }
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let songCell = cell as! SongCell
        songCell.setupCell(atIndex: indexPath.row)
        songCell.populate(withSong: self.party?.songs?[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return indexPath.row == self.longPressedIndex ? .delete : .none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row == self.longPressedIndex
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 180.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRect(x: 0,y: 0,width: tableView.frame.size.width, height: 180))
        footer.backgroundColor = .clear
        return footer
    }
   
}
