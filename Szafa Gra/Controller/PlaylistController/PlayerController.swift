//
//  PlayerController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 17/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import AVKit
import youtube_ios_player_helper
import MediaPlayer

class PlayerController: NSObject {
    var mediaPlayer: AVPlayer?
    var subscribedToPlayerState = false
    private var spotifyTimer: Timer?
    var state: PlayerState = .notStarted
    
    var spotifyDelegate: SpotifyPlayerDelegate! {
        return PlaylistViewController.shared.spotifyDelegate
    }
    
    var appRemote: SPTAppRemote? {
        get {
            return AppDelegate.shared?.appRemote
        }
    }
    
    var party: Party? {
        get{
            return PlaylistViewController.shared.party
        }
        set{
            PlaylistViewController.shared.party = newValue
        }
        
    }
    
    var currentSong: Song? {
        return PlaylistViewController.shared.currentSong
    }
    
    var playlistController: PlaylistViewController!{
        return PlaylistViewController.shared!
    }
    
    var defaultCallback: SPTAppRemoteCallback {
        get {
            return {[weak self] _, error in
                if(error != nil){
                   // self?.appRemote?.disconnect()
                }
            }
        }
    }
    
    var spotifyPlayerState : SPTAppRemotePlayerState?
    
    // MARK: Player controls methods:
    
    @objc func play(){
        self.play(self.currentSong)
    }
    
    func play(_ song: Song?){
        self.state = .playing
        guard self.party?.type == .hosted else {return}
        
        RootViewController.shared?.controlButtons?[2].isSelected = true
        self.appRemote?.playerAPI?.pause(nil)
        switch song?.source
        {
        case .local(_)? : self.playLocalSong(song)
        case .youtube(_)? : self.playYoutubeSong(song)
        case let .spotify(link: uri)? : self.playSpotifySong(withURI: uri)
        case _ : return
        }
    }
    
    func playLocalSong(_ song: Song?){
        guard case let SongSource.local(link: link)? = song?.source else {return}
        self.appRemote?.disconnect()
        if(self.mediaPlayer == nil){
            self.prepareMediaPlayer(forSong: song)
        }
        else if(urlOfCurrentlyPlayingInPlayer(self.mediaPlayer!) != link){
            self.changeSongInMediaPlayer(forSong: song!)
        }
        
        self.mediaPlayer?.play()
        playlistController.updateBackground(forSong:song)
    }
    
    func playYoutubeSong(_ song: Song?){
        guard case let SongSource.youtube(videoId: videoId)? = song?.source, let id = videoId else {return}
        self.mediaPlayer = nil
        self.appRemote?.disconnect()
        if(playlistController.youtubePlayerView.videoUrl() == nil){
            playlistController.youtubePlayerView.delegate = playlistController.youtubePlayerDelegate
            playlistController.youtubePlayerView.load(withVideoId: id, playerVars: [
                "playsinline" : 1, "autoplay" : 1, "origin" : "http://www.youtube.com"])
        }
        else if ( !playlistController.youtubePlayerView.videoUrl()!.absoluteString.contains(id)){
            playlistController.youtubePlayerView.cueVideo(byId: id, startSeconds: Float(0.0), suggestedQuality: YTPlaybackQuality.default)
            playlistController.youtubePlayerView.playVideo()
        }else{
            playlistController.youtubePlayerView.playVideo()
        }
        
        playlistController.youtubePlayerView.alpha = 1.0
        
        //self.startObservingYoutubeVideo()
    }
    func stopYoutube(){
        RootViewController.shared?.controlButtons?[2].isSelected = false
        playlistController.youtubePlayerView.stopVideo()
    }
    
    func stop(){
        guard self.party?.type == .hosted else {return}
        switch self.currentSong?.source
        {
        case .local(_)? :  self.pause();
        case .youtube(_)? : self.stopYoutube()
            
        case .spotify(_)? : self.pause()
        case _ : return
        }
        self.state = .stopped
    }
    
    @objc func pause(){
        guard self.party?.type == .hosted else {return}
        self.state = .paused
        RootViewController.shared?.controlButtons?[2].isSelected = false
        switch self.currentSong?.source
        {
        case .local(_)? :  self.mediaPlayer?.pause()
        case .youtube(_)? : playlistController.youtubePlayerView.pauseVideo()
        case .spotify(_)? : self.pauseSpotify()
        case _ : return
        }
        
    }
    func end(){
        self.stop()
        self.rewind(toPercent: 0.0)
        self.state = .ended
    }
    
    @objc func nextSong(){
        guard self.party?.type == .hosted, self.party!.songIndex+1 < self.party?.songs?.count ?? 0, (self.party?.songs?[self.party!.songIndex+1]) != nil else {return}
        
        var nextIndex = self.party!.songIndex+1
        if(self.party!.songs![nextIndex].source!.isSpotify)
        {
            if(spotifyTracksDisabled){
                let remaining = self.party!.songs![nextIndex...]
                let ix = remaining.firstIndex {
                    if ($0.source!.isSpotify){
                        return false
                    }
                    else{
                        return true
                    }
                }
                if (ix != nil)
                {
                    nextIndex = ix!
                }
                else{
                    return
                }
            }
        }
        
        
        self.selectSong(atIndex: nextIndex)
        
    }
    
    @objc func previousSong(){
        
        guard self.party?.type == .hosted, self.party!.songIndex > 0, (self.party?.songs?[self.party!.songIndex-1]) != nil else {return}
        
        var nextIndex = self.party!.songIndex-1
        
        if(self.party!.songs![nextIndex].source!.isSpotify)
        {
            if(spotifyTracksDisabled){
                let remaining = self.party!.songs![...nextIndex]
                let ix = remaining.lastIndex {
                    if $0.source!.isSpotify {
                        return false
                    }
                    else{
                        return true
                    }
                }
                if (ix != nil)
                {
                    nextIndex = ix!
                }
                else{
                    return
                }
            }
        }
        
        
        self.selectSong(atIndex: nextIndex)
    }
    
    func selectSong(atIndex index: Int){
        
        guard index >= 0, index < self.party?.songs?.count ?? 0 else {return}
        self.stop()
        
        let song = self.party?.songs?[index]
        playlistController.tableView.selectRow(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .none)
        self.party!.songIndex = index
        self.updatePlayerView(withSong:song)
        
        self.play(song)
        
        if(self.party?.type == .hosted){
            BluetoothManager.updatePartyOnRemoteDevices(self.party!)
        }
        
    }
    
    func handleFirstSongsAdded(){
        if(playlistController.tableView.indexPathForSelectedRow == nil){
            playlistController.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .none)
            self.handleSelection(atIndex: 0)
        }
    }
    
    func handleSelection(atIndex index: Int){
        let playButton = RootViewController.shared?.controlButtons?[2]
        playButton?.isSelected = true
        self.selectSong(atIndex: index)
    }
    
    func updatePlayerView(withSong song: Song?){
        guard let song = song else {return}
        playlistController.artistLabel.text = song.artist!
        playlistController.titleLabel.text = song.name!
        playlistController.sourceView.image = UIImage(named: song.source?.iconName ?? "songPlaceholder")
        
        playlistController.timeLabel.text = convertToString(duration: song.duration)
        
        playlistController.albumCoverView.image = song.albumCover ?? UIImage(named:"songPlaceholder")
        self.updateRemoteControl(withSong: song)
        self.updateProgressBar(withPercent: 0.0)
        
    }
    
    func updateRemoteControl(withSong song: Song){
        var nowPlayingInfo = [MPMediaItemPropertyArtist : song.artist!,  MPMediaItemPropertyTitle : song.name!] as [String : Any]
        
        nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: CGSize(width: 100, height: 100), requestHandler: { (size) -> UIImage in
            song.albumCover ?? UIImage(named: "songPlaceholder")!
        })
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
    
    func updatePlaybackTime(_ time: Double?){
        guard self.currentSong != nil, let time = time else {return}
        if(self.currentSong!.source!.isSpotify && self.currentSong!.duration == 0)
        {
            guard self.spotifyPlayerState != nil else {return}
            let realSpotifyDuration = Int(self.spotifyPlayerState!.track.duration)
            self.party!.songs![self.party!.songIndex].duration = realSpotifyDuration/1000
        }
        let percent = time / Double(self.party!.songs![self.party!.songIndex].duration!)
        self.updateProgressBar(withPercent: percent)
        playlistController.timeLabel.text = convertToString(duration: Int(time))
    }
    
    
    func updateProgressBar(withPercent percent: Double){
        guard percent >= 0.0, percent <= 1.0 else {return}
        playlistController.progressBarWidth.constant = playlistController.view.frame.size.width * CGFloat(percent)
        UIView .animate(withDuration: percent < 0.01 ? 1.0 : 0.2) {
            self.playlistController.progressBar.superview?.layoutIfNeeded()
        }
    }
    
    func prepareMediaPlayer(forSong song:Song?){
        guard let song = song else{return}
        // Play the item using AVPlayer
        if case let .local(link:url) = song.source! {
            self.connectRemoteControls()
            self.activateAudioSession()
            if(url != nil){
                
                let playerItem = AVPlayerItem(url: URL(string: url!)!)
                let player = AVPlayer(playerItem: playerItem)
                
                NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlayingSong(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                player.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main, using: { (time) in
                    if player.currentItem?.status == .readyToPlay {
                        let currentTime = CMTimeGetSeconds(player.currentTime())
                        
                        
                        self.updatePlaybackTime(Double(currentTime))
                    }
                })
                self.mediaPlayer = player
            }
            else{
                self.mediaPlayer?.pause()
            }
        }
    }
    
    func activateAudioSession(){
        UIApplication.shared.isIdleTimerDisabled = true
        let appInBackground = UIApplication.shared.applicationState == .background
        print(appInBackground ? "In background" : "is active")
        let options: AVAudioSession.CategoryOptions = appInBackground ? [.mixWithOthers, .interruptSpokenAudioAndMixWithOthers] : [.allowBluetooth]
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: options)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
    }
    
    func connectRemoteControls(){
        DispatchQueue.once{
            let center = MPRemoteCommandCenter.shared()
            center.nextTrackCommand.isEnabled = true
            center.nextTrackCommand.addTarget(self, action: #selector(nextSong))
            center.previousTrackCommand.isEnabled = true
            center.previousTrackCommand.addTarget(self, action: #selector(previousSong))
            center.playCommand.isEnabled = true
            center.playCommand.addTarget(self, action: #selector(play as ()->()))
            center.pauseCommand.isEnabled = true
            center.pauseCommand.addTarget(self, action: #selector(pause))
        }
            UIApplication.shared.beginReceivingRemoteControlEvents()
        
    }
    
    func changeSongInMediaPlayer(forSong song: Song)
    {
        if case let .local(link:url) = song.source! {
            if(url != nil){
                let playerItem = AVPlayerItem(url: URL(string: url!)!)
                self.mediaPlayer?.replaceCurrentItem(with: playerItem)
            }
        }
    }
    
    func rewind(toPercent percent: Double){
        
        switch self.currentSong?.source
        {
        case .local(_)? :  self.mediaPlayer?.seek(to: CMTimeMake(value: Int64(100.0*percent*Double(self.currentSong!.duration!)), timescale: 100))
        case .youtube(_)? :
            playlistController.youtubePlayerView.seek(toSeconds: Float(percent*Double(self.currentSong!.duration!)), allowSeekAhead: true)
            if(playlistController.youtubePlayerView.playerState() == .paused){
                self.updateProgressBar(withPercent: percent)
            }
            
        case .spotify(_)?: self.rewindSpotify(percent)
        case _ : return
        }
    }
    
    
    func rewindSpotify(_ percent: Double){
        self.updateProgressBar(withPercent: percent)
        guard let remote = self.appRemote else {return};
        
        remote.playerAPI?.seek(toPosition: Int(percent*(1000.0*Double(self.currentSong!.duration!))), callback: defaultCallback)
        
    }
    
    func handleEndOfSong(){
        if(self.party!.songIndex == self.party!.songs!.count - 1){
            
            //            let playButton = RootViewController.shared?.controlButtons?[2]
            //            playButton?.isSelected = false
            self.end()
            
            
        }
        else{
            self.nextSong()
        }
    }
    
    func removeSong(atIndex index: Int){
        if(self.party!.songIndex == index){
            self.stop()
        }
        playlistController.tableView.isEditing = false
        self.party?.songs?.remove(at: index)
        playlistController.tableView.beginUpdates()
        playlistController.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        
        playlistController.tableView.endUpdates()
        if(self.party!.songs!.count > 0){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                if(index < self.party!.songIndex){
                    self.party!.songIndex -= 1
                    self.playlistController.tableView.selectRow(at: IndexPath(row:self.party!.songIndex, section: 0), animated: true, scrollPosition: .none)
                }
                else if(index == self.party!.songIndex){
                    self.party!.songIndex = (self.party!.songIndex)%self.party!.songs!.count
                    self.handleSelection(atIndex: self.party!.songIndex)
                }else{
                    self.playlistController.tableView.selectRow(at: IndexPath(row:self.party!.songIndex, section: 0), animated: true, scrollPosition: .none)
                }
            }
        }
        else{
            playlistController.handleNoSongs()
        }
        
        if(self.party?.type == .hosted){
            BluetoothManager.updatePartyOnRemoteDevices(self.party!)
        }
        
    }
    
    func playSpotifySong(withURI uri: String?){
        guard let uri = uri, let appRemote = AppDelegate.shared?.appRemote else {
            return}
        
        self.mediaPlayer = nil
        playlistController.updateBackground(forSong:self.currentSong)
        
        
        if(!appRemote.isConnected && isSpotifyPremiumUser){
            print("AUTHORIZE AND PLAY URI")
            appRemote.authorizeAndPlayURI(uri) // resume spotify stream after playing from other source
        }
        else{
            let newSong = self.spotifyPlayerState == nil || (self.spotifyPlayerState != nil && self.spotifyPlayerState!.track.uri != uri)
            if (!spotifyAuthorized) {
                
                if(!appRemote.authorizeAndPlayURI(uri)){
                    playlistController.showSpotifyAppStoreInstall()
                }
                else{
                    spotifyAuthorized = true
                }
                
            }
            else if(!isSpotifyPremiumUser){
                
            }
            else if(newSong){
                print("PLAY NEW SONG")
                self.appRemote?.playerAPI?.play(uri, callback: defaultCallback)
            }
            else if(self.spotifyPlayerState!.isPaused){
                print("RESUME")
                self.appRemote?.playerAPI?.resume(defaultCallback)
            }
        }
    }
    
    func pauseSpotify(){
        guard let state = self.spotifyPlayerState else {return}
        if(!state.isPaused){
            self.appRemote?.playerAPI?.pause(defaultCallback)
        }
    }
    
    @objc func playerDidFinishPlayingSong(_ song: AVPlayerItem?){
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlayingSong(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        self.handleEndOfSong()
    }

    
    func updateSpotify(playerState:SPTAppRemotePlayerState){
        
        self.spotifyPlayerState = playerState
        let title = playerState.track.name
        let songIsNotOnPlaylist = (self.party!.songs!.filter{
            $0.name == title
        }.count == 0)
        if(songIsNotOnPlaylist){
            if(!self.playlistController.spotifyDelegate.isLost){
                print("elo")
                self.playlistController.spotifyDelegate.isLost = true
                self.nextSong()
               // self.appRemote?.disconnect()
            }
        }
        else{
            self.playlistController.spotifyDelegate.isLost = false
        }
        
//        if(self.currentSong != nil && self.currentSong!.source!.isSpotify){
//            if(playerState.isPaused && self.state == .playing){
//                self.pause()
//            }
//            else if(!playerState.isPaused && self.state != .playing){
//                self.play()
//            }
//        }
    }
    
    private func getSpotifyPlayerState(_ completion: @escaping (SPTAppRemotePlayerState) -> ()){
        guard let appRemote = self.appRemote else {return}
        appRemote.playerAPI?.getPlayerState({ (result, error) in
            guard error == nil else { return }
            
            self.spotifyPlayerState = result as? SPTAppRemotePlayerState
            completion(self.spotifyPlayerState!)
        })
    }
    
    func observeSpotifyPlaybackTime(){
        if(self.spotifyTimer != nil){
            self.stopObservingSpotifyPlaybackTime()
        }
        self.spotifyTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
            self.appRemote?.playerAPI?.getPlayerState({ (state, error) in
                self.getSpotifyPlayerState({ (state) in
                    let time = Double(state.playbackPosition) / 1000.0
                    print(time)
                    self.updatePlaybackTime(time)
                    if(abs(state.playbackPosition - Int(state.track.duration)) < 2000)
                    {
                        self.stopObservingSpotifyPlaybackTime()
                        self.handleEndOfSong()
                    }
                })
                
            })
            
        })
        
        
    }
    
    func stopObservingSpotifyPlaybackTime(){
        self.spotifyTimer?.invalidate()
        self.spotifyTimer = nil
    }
    
    
    
    
    
    func subscribeToPlayerState() {
        guard let appRemote = self.appRemote, !subscribedToPlayerState else { return }
        appRemote.playerAPI!.delegate = self.spotifyDelegate
        appRemote.playerAPI?.subscribe { (_, error) -> Void in
            guard error == nil else { return }
            self.subscribedToPlayerState = true
        }
    }
    
    func unsubscribeFromPlayerState() {
        guard (subscribedToPlayerState) else { return }
        self.appRemote?.playerAPI?.unsubscribe { (_, error) -> Void in
            guard error == nil else { return }
            self.subscribedToPlayerState = false
        }
    }
    
    
    
}

enum PlayerState{
    case notStarted
    case playing
    case paused
    case stopped
    case ended
}
