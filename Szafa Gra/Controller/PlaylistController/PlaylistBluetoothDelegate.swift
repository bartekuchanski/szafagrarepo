//
//  PlaylistBluetoothDelegate.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 17/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import BluetoothKit

class PlaylistBluetoothDelegate: NSObject, BKPeripheralDelegate, BKRemotePeripheralDelegate, BKCentralDelegate, BKRemotePeerDelegate {
    
    var playlistController: PlaylistViewController!{
        return PlaylistViewController.shared
    }
    
    var party: Party?{
        get{
            return self.playlistController.party
        }
        set{
            self.playlistController.party = newValue
        }
    }

    
    
    
    
    // MARK: BKPeripheralDelegate methods
    
    func peripheral(_ peripheral: BKPeripheral, remoteCentralDidConnect remoteCentral: BKRemoteCentral) {
        guard let _ = self.party else {return}
        remoteCentral.delegate = self
        let dict: [String : String] = ["result" : "ready"]
        
        peripheral.sendData(dataFromDict(dict)!, toRemotePeer: remoteCentral) { (_, _, _) in}
        
        print("central connected: \(remoteCentral)")
    }
    
    
    func peripheral(_ peripheral: BKPeripheral, remoteCentralDidDisconnect remoteCentral: BKRemoteCentral) {
        if let controller = playlistController.presentedViewController as? ConnectionRequestController, let peer = controller.peer, peer.identifier.uuidString == remoteCentral.identifier.uuidString{
            playlistController.dismiss(animated: true, completion: nil)
        }
        self.party!.connectedUsers?[remoteCentral.identifier.uuidString] = nil
        
    }
    
    // BKRemotePeripheralDelegate methods:
    
    func remotePeer(_ remotePeer: BKRemotePeer, didSendArbitraryData data: Data) {
        guard let party = self.party else {return}
        if(party.type == .remote){
            if let dict = dictFromData(data), dict["result"] as? String == "denied" {
                playlistController.presentAlert(title: "Access denied", message: "Host of this party denied you the access."){
                    self.playlistController.exitParty()
                }
            }
            else {
                DispatchQueue.main.async{
                    guard let updatedParty = partyFromData(data) else {return}
                    RootViewController.shared?.stateButton?.isSelected = true
                    self.party = updatedParty
                    self.playlistController.updateTableAfterAddingSongs(firstSongs: false)
                    self.playlistController.playerController.updatePlayerView(withSong: self.playlistController.currentSong)
                }
            }
        }
        else{
            guard let dict = dictFromData(data), dict["nickname"] != nil else { playlistController.updatePlaylistWithSongsData(data, fromPeer: remotePeer) // if song received
                if(self.playlistController.playerController.state == .ended || self.playlistController.playerController.state == .notStarted){
                    self.playlistController.playerController.selectSong(atIndex: self.playlistController.party!.songs!.count-1)
                }
                return
            }
            // if connection request received
            playlistController.presentConnectionRequestSheet(forPeer: remotePeer, withDict: dict)
        }
    }
    
    func remotePeripheral(_ remotePeripheral: BKRemotePeripheral, didUpdateName name: String) {
        
    }
    
    func remotePeripheralIsReady(_ remotePeripheral: BKRemotePeripheral){
        
    }
    
    // MARK: BKCentralDelegate:
    
    func central(_ central: BKCentral, remotePeripheralDidDisconnect remotePeripheral: BKRemotePeripheral) {
        
        RootViewController.shared?.stateButton?.isSelected = false
    }
    
}




