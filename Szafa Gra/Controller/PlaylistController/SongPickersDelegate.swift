//
//  SongPickersDelegate.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 17/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import MediaPlayer
import SpotifyKit

class SongPickersDelegate: NSObject, SourcePickerViewDelegate, MPMediaPickerControllerDelegate, YoutubePickerViewControllerDelegate, SpotifyPickerDelegate {
    
    var party: Party? {
        get{
            return PlaylistViewController.shared.party
        }
        set{
            PlaylistViewController.shared.party = newValue
        }
    }
    
    var mediaPicker: MPMediaPickerController?
    

    // MARK: MPMediaPickerControllerDelegate methods:
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        
        if(self.party!.type! == .hosted){
            let firstSongsAdded = mediaItemCollection.items.count > 0 && self.party!.songs!.count == 0
            
            for item in mediaItemCollection.items
            {
                PlaylistViewController.shared.addMusicAppSong(withItem: item)
            }
            
            PlaylistViewController.shared.updateTableAfterAddingSongs(firstSongs: firstSongsAdded)
        }
        else{
            
            
        }
        
        mediaPicker.dismiss(animated: true, completion: nil)
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        mediaPicker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: SourcePickerViewDelegate methods:
    
    func sourcePickerButtonClicked(tag: Int) {
        PlaylistViewController.shared.addClicked(withType: SongSource.caseFromButtonTag(tag))
    }
    
    // MARK: YoutubePickerViewControllerDelegate methods
    
    func youtubePicker(picker: YoutubePickerViewController, didPickVideoWithInfo info:  Dictionary<String,Any>) {
        PlaylistViewController.shared.addYoutubeSong(withInfo: info)
        RootViewController.stopLoading()
    }
    
    func youtubePicker(picker: YoutubePickerViewController, didFailPickingVideoWithError error:YoutubeError){
        RootViewController.stopLoading()
        
    }
    
    
    // MARK: SpotifyPickerDelegate methods:
    
    func spotifyPickerDidPick(track: SpotifyTrack) {
        PlaylistViewController.shared.addSpotifyTrack(track)
    }

}
