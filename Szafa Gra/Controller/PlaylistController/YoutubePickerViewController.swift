//
//  YoutubePickerViewController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 02/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import WebKit


enum YoutubeError {
    case wrongURL
    case noConnection
    case accessDenied
}

class YoutubePickerViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    
    private var url: URL?
    private var acceptButton: UIBarButtonItem?
    private var urlObserver: NSKeyValueObservation?
    
    var delegate: YoutubePickerViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.webView.navigationDelegate = self
        self.loadWebsite(withURL: URL(string:"https://youtube.com"))
//        self.acceptButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addClicked))
//        self.acceptButton?.isEnabled = false
//        self.navigationItem.rightBarButtonItem = self.acceptButton
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.stopObservingURL()
    }
    
    func loadWebsite(withURL url: URL?){
        self.startObservingURL()
        self.url = url
        guard let url = url else {return}
        let request = URLRequest(url: url) 
        self.webView.load(request)
        
    }
    
    func startObservingURL(){
        if(self.urlObserver != nil){
            self.stopObservingURL()
        }
        
        self.urlObserver = self.webView.observe(\WKWebView.url) { (webview, change) in
            DispatchQueue.main.async{self.checkURL()}
            
        }

    }
    
    func stopObservingURL(){
        self.urlObserver?.invalidate()
        self.urlObserver = nil
    }
    
    func checkURL(){
        guard let url = self.webView.url else {return}
        if(getYoutubeId(youtubeUrl: url.absoluteString) != nil){
            self.stopObservingURL()
            self.addClicked()
        }
    }
    
    

    @objc func addClicked(){
        
        defer{DispatchQueue.main.async{self.navigationController?.popViewController(animated: true)}}
        RootViewController.startLoading()
        let urlString = self.webView?.url?.absoluteString
        guard urlString != nil, urlString!.contains("youtube"), let youtubeId = getYoutubeId(youtubeUrl: urlString!) else {return}
        
        getInfoForVideo(withId: youtubeId, completion: { (dict) in
            
            
            DispatchQueue.main.async{
                self.delegate?.youtubePicker(picker: self, didPickVideoWithInfo:dict)
            }
            
        }, error:{ (error) in
            DispatchQueue.main.async{
                self.delegate?.youtubePicker(picker: self, didFailPickingVideoWithError: error)
            }
        })
    
    }
}


