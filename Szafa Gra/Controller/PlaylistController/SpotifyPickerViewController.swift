//
//  SpotifyPickerViewController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 05/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import SpotifyKit

class SpotifyPickerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    private var tracks: [SpotifyTrack] = []
    weak var delegate: SpotifyPickerDelegate?

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.title = "Spotify"
        self.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.textField.attributedPlaceholder = NSAttributedString(string: "Search on spotify", attributes: [.font : UIFont(name: "HelveticaNeue-Light", size: 14) as Any, .foregroundColor : UIColor.myGreen])
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.textField.becomeFirstResponder()
        
        self.loadTracks(forPhrase: "A")
    
    }
    
    func loadTracks(forPhrase phrase: String?){
        guard let phrase = phrase else {return}
        AppDelegate.shared!.spotifySearchManager.find(SpotifyTrack.self, phrase) { tracks in
            // Tracks is a [SpotifyTrack] array
            self.tracks = tracks
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SpotifyTrackCell", for: indexPath) as UITableViewCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.black.withAlphaComponent(indexPath.row%2==0 ? 0.1 : 0.2)
        
        let label = cell.contentView.subviews.first as! UILabel
        
        let track = self.tracks[indexPath.row]
        label.text = "\(track.artist.name) - \(track.name)"
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let track = self.tracks[indexPath.row]
        self.delegate?.spotifyPickerDidPick(track: track)
        self.navigationController?.popViewController(animated: true)
    }
   
    @objc func textFieldDidChange(_ textField: UITextField){
        self.loadTracks(forPhrase: textField.text)
    }
}


