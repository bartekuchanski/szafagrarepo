//
//  YoutubePlayerDelegate.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 17/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import youtube_ios_player_helper
class YoutubePlayerDelegate: NSObject, YTPlayerViewDelegate {
    var previousYoutubeState : YTPlayerState = .unknown
    
    // MARK: YTPlayerViewDelegate methods:
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.playVideo()
    }
    
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        print(error)
    }
    
    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
        PlaylistViewController.shared.playerController.updatePlaybackTime(Double(playTime))
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        
        if(self.previousYoutubeState == .buffering && state == .unstarted){
            playerView.playVideo()
        }
        if(state == .ended){
            PlaylistViewController.shared.playerController.handleEndOfSong()
        }
    }

}
