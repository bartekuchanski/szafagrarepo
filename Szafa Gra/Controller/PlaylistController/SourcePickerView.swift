//
//  SourcePickerView.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 02/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

class SourcePickerView: PinnedView {

    weak var delegate: SourcePickerViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    
    convenience init(options: [SongSource]){
        self.init(frame: SourcePickerView.calculateRect(forOptions: options))
        self.verticalSpacing = 45.0
        self.backgroundColor = .clear
        let blur = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        blur.frame = self.frame
        self.addSubview(blur)
        for (ix, option) in options.enumerated(){
            let button = UIButton(frame: CGRect(x: 0.0, y: CGFloat(ix)*60.0, width: self.frame.width, height: 60.0))
            button.tag = option.buttonTag
            button.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
            button.setTitleShadowColor(.white, for: .selected)
            button.setAttributedTitle(NSAttributedString(string: option.buttonTitle, attributes: [NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Light", size: 14.0) as Any, NSAttributedString.Key.foregroundColor : UIColor.myGreen]), for: .normal)
            button.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.addSubview(button)
        }
        self.alpha = 0.0
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 8.0
        
    }

    static func calculateRect(forOptions options: [Any]) -> CGRect{
        return CGRect(x: 0, y: 0, width: UIApplication.shared.keyWindow!.frame.size.width-30.0, height: CGFloat(options.count)*60.0)
    }
    
    func show(attachedToView view: UIView?, inSuperView superview: UIView?, delegate: SourcePickerViewDelegate){
        self.show(attachedToView: view, inSuperView: superview)
        self.delegate = delegate
        
    }
    
    
    
    
    @objc func buttonClicked(_ sender: UIButton){
        sender.backgroundColor = UIColor.black.withAlphaComponent(0.4)
     //   self.delegate?.sourcePickerButtonClicked(tag: sender.tag, sourcePicker: self)
        sender.isEnabled = false
    }
}

class PinnedView: UIView{
    
    var verticalSpacing: CGFloat = 15.0
    var verticalPadding: CGFloat = 15.0
    var horizontalPadding: CGFloat = 15.0
    
    func show(attachedToView view: UIView?, inSuperView superview: UIView?){
        //let frame = firstView.convert(buttons.frame, from:secondView)
        guard let view = view, let superview = superview else{return}
        if(self.superview !== superview){
            self.frame = CGRect(origin:CGPoint(x:CGFloat.minimum(view.center.x-CGFloat(0.5)*self.frame.size.width, superview.frame.size.width-self.frame.size.width-self.horizontalPadding), y: view.frame.origin.y - self.verticalPadding - self.frame.size.height), size: self.frame.size)
            superview.addSubview(self)
            let center = self.center
            UIView.animate(withDuration: 0.3) {
                self.center = CGPoint(x:center.x, y: center.y - self.verticalSpacing)
                self.alpha = 1.0
            }
        }
        
    }
    
    func hide(){
        let center = self.center
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut,  animations: {
            self.center = CGPoint(x:center.x, y: center.y + 45)
            self.alpha = 0.0
        }) { (completed) in
            self.removeFromSuperview()
        }
    }
    
}



