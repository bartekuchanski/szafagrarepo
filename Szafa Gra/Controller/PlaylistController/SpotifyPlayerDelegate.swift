//
//  SpotifyPlayerDelegate.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 17/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit


class SpotifyPlayerDelegate: NSObject, SPTAppRemotePlayerStateDelegate, SPTAppRemoteUserAPIDelegate {
    
    var isLost: Bool = false
    
    var playerController: PlayerController! {
        return PlaylistViewController.shared.playerController
    }

    
    // MARK: SPTAppRemotePlayerStateDelegate methods:
    
    func playerStateDidChange(_ playerState: SPTAppRemotePlayerState) {
        if(playerState.isPaused)
        {
            self.playerController.stopObservingSpotifyPlaybackTime()
        }
        else{
            self.playerController.observeSpotifyPlaybackTime()
        }
        self.playerController.updateSpotify(playerState: playerState)
    }
    
    // MARK: SPTAppRemoteUserAPIDelegate methods:
    
    func userAPI(_ userAPI: SPTAppRemoteUserAPI, didReceive capabilities: SPTAppRemoteUserCapabilities) {
        print(capabilities.canPlayOnDemand ? "premium": "free")
    }
    

}
