//
//  PlaylistViewController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 29/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import MediaPlayer
import youtube_ios_player_helper
import SpotifyKit
import StoreKit
import BluetoothKit

let cellIdentifier = "SongCell"
var spotifyTracksDisabled = false





final class PlaylistViewController: UIViewController, DataViewController, SKStoreProductViewControllerDelegate {
    
    // MARK: Delegates:
    
    var playlistDataSource: PlaylistDataSource!
    var playlistDelegate: PlaylistDelegate!
    var songPickersDelegate: SongPickersDelegate!
    var youtubePlayerDelegate: YoutubePlayerDelegate!
    var spotifyDelegate: SpotifyPlayerDelegate!
    var bluetoothDelegate: PlaylistBluetoothDelegate!
    var playerController: PlayerController!
    
    
    // MARK: IBOutlets
    
    @IBOutlet weak var youtubePlayerView: YTPlayerView!
    @IBOutlet weak var progressBarWidth: NSLayoutConstraint!
    @IBOutlet weak var progressBar: UIView!
    @IBOutlet weak var sourceView: UIImageView!
    @IBOutlet weak var albumCoverView: UIImageView!
    @IBOutlet weak var playerView: UIVisualEffectView!
    @IBOutlet weak var playerBackgroundView: UIImageView!
    @IBOutlet weak var backgroundView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hostY: NSLayoutConstraint!
    @IBOutlet weak var playerHeight: NSLayoutConstraint!
    @IBOutlet weak var joinY: NSLayoutConstraint!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var artworkBackgroundView: UIImageView!
    
    // MARK: Private properties
    
    private var backgroundTimer : Timer?
    private var backgrounds = ["partyBackground1", "partyBackground2", "partyBackground3", "partyBackground4"]
    
    
    
    
    private var connectionRequests: [[String:Any]] = []
    
    // MARK: Public properties
    
    var party: Party?
    var pageIndex = 0
    static var shared: PlaylistViewController!
    
    
    
    // MARK: Computed properties
    
    var currentSong: Song? {
        guard let songs = self.party?.songs, songs.count != 0 else {return nil}
        
        return party!.songIndex >= 0 && party!.songIndex < songs.count ? songs[party!.songIndex] : nil
    }
    
    // MARK: UIViewController methods:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PlaylistViewController.shared =  self
        self.connectDelegates()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        var completion: (()->())?
        self.prepareViews()
        if var party = self.party, party.type! == .saved{
            party.type = .hosted
            self.party = party
            completion = {
                self.updateTableAfterAddingSongs(firstSongs: false)
                self.playerController.updatePlayerView(withSong: self.currentSong)
            }
                
        }
        
        self.showPartyIfPossible(completion: completion)
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.prepareDisappearing()
    }
    
    func connectDelegates(){
        self.playerController = PlayerController()
        self.playlistDataSource = PlaylistDataSource()
        self.playlistDelegate = PlaylistDelegate()
        self.tableView.delegate = self.playlistDelegate
        self.tableView.dataSource = self.playlistDataSource
        self.songPickersDelegate = SongPickersDelegate()
        self.youtubePlayerDelegate = YoutubePlayerDelegate()
        self.spotifyDelegate = SpotifyPlayerDelegate()
        self.bluetoothDelegate = PlaylistBluetoothDelegate()
        
        
    }
    
    func prepareDisappearing(){
        if(self.party != nil){
            if case SongSource.youtube(videoId:_)? = self.currentSong?.source{
                self.playerController.pause()
            }
            
            RootViewController.shared!.showHideControls(show: false)
           
        }
    }
    
    func prepareViews(){
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if(self.party != nil)
        {
            RootViewController.shared!.showHideControls(show: true)
        }
    }
    
    
    // MARK: PlaylistViewController methods:
    
    func hostParty(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CreateViewController") as! CreateViewController
        viewController.partyController = self
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(viewController, animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func searchForParties(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        viewController.partyController = self
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(viewController, animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    func hideOptions(completion:(()->())?){
        self.hostY.constant = -200
        self.joinY.constant = 20
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            for case let button as UIButton in self.view.subviews where button.tag == 1
            {
                button.alpha = 0.0
            }
            self.view.layoutIfNeeded()
        }) { (completed) in
            completion?()
        }
    }
    
    func showOptions(completion:(()->())?){
        self.startBackgroundAnimation()
        self.joinY.constant = -85
        self.hostY.constant = -85
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            for case let button as UIButton in self.view.subviews where button.tag == 1
            {
                button.alpha = 1.0
                self.tableView.alpha = 0.0
            }
            self.view.layoutIfNeeded()
        }) { (completed) in
            completion?()
        }
    }
    
    func showPartyIfPossible(completion: (()->())?){
        if(self.party == nil){
            self.showOptions(completion: (completion))
        }
        else{
            self.showPlaylist(completion: (completion))
        }
    }
    
    func showPlaylist(completion:(()->())?){
        
        self.playerView.alpha = 1.0
        self.playerBackgroundView.alpha = 1.0
        self.albumCoverView.superview!.alpha = (self.party?.songs != nil && self.party!.songs!.count != 0) ? 1.0 : 0
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            
            self.tableView.alpha = 1.0
            
        }) { (completed) in
            completion?()
            ControlsConfiguration: do {
                DispatchQueue.once{
                    let more = RootViewController.shared?.controlButtons?[0]
                    let backward = RootViewController.shared?.controlButtons?[1]
                    let play = RootViewController.shared?.controlButtons?[2]
                    let forward = RootViewController.shared?.controlButtons?[3]
                    let add = RootViewController.shared?.controlButtons?[4]
                   
                    
                    more?.addTarget(self, action: #selector(self.moreClicked(_:)) , for: .touchUpInside)
                    backward?.addTarget(self, action: #selector(self.backwardClicked(_:)) , for: .touchUpInside)
                    play?.addTarget(self, action: #selector(self.playClicked(_:)) , for: .touchUpInside)
                    forward?.addTarget(self, action: #selector(self.forwardClicked(_:)) , for: .touchUpInside)
                    add?.addTarget(self, action: #selector(self.addClicked(_:)) , for: .touchUpInside)
                }
            }
        }
    }
    
    func hidePlaylist(completion: (()->())?){
        RootViewController.shared?.showHideControls(show: false)
        self.playerBackgroundView.alpha = 0.0
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            
            self.tableView.alpha = 0.0
            
            self.playerView.alpha = 0.0
            
            self.view.layoutIfNeeded()
        }) { (completed) in
            self.tableView.reloadData()
            completion?()
        }
    }
    
    
    
    func handleSpotifyFreeAccount(){
        //        RootViewController.shared?.controlButtons?[2].isSelected = false
        self.playerController.pause()
        if(!spotifyTracksDisabled){
            spotifyTracksDisabled = true
            self.presentAlert(title: "Spotify Premium", message: "Playing songs from Spotify require a premium user account. Please log into another account and try again."){
                self.tableView.reloadData()
            }
        }
        else{
            self.playerController.handleEndOfSong()
        }
    }
    
    // MARK: Background animations methods:
    
    
    func startBackgroundAnimation(){
        if(self.backgroundTimer != nil){
            self.stopBackgroundAnimation()
        }
        var index = 0
        self.backgroundTimer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true, block: { (timer) in
            
            self.changeBackground(index:index)
            index = (index+1)%self.backgrounds.count
        })
        
    }
    
    func stopBackgroundAnimation(){
        self.backgroundTimer?.invalidate()
        self.backgroundTimer = nil
    }
    
    func changeBackground(index: Int){
        UIView.transition(with: self.backgroundView,
                          duration: 0.75,
                          options: .transitionCrossDissolve,
                          animations: { self.backgroundView.image = UIImage(named:  self.backgrounds[index])
        }, completion: nil)
        
        UIView.transition(with: self.playerBackgroundView, duration: 0.75, options: .transitionCrossDissolve, animations: {self.playerBackgroundView.image = UIImage(named:  self.backgrounds[index])}, completion: nil)
        
    }
    
    func updateBackground(forSong song: Song?)
    {
        guard let song = song, let artwork = song.albumCover else {
            self.artworkBackgroundView.image = nil
            self.backgroundView.alpha = 0.1
            self.startBackgroundAnimation()
            self.youtubePlayerView.alpha = 0.0
            return
        }
        self.artworkBackgroundView.image = artwork
        self.playerBackgroundView.image = artwork
        self.backgroundView.alpha = 0.0
        self.youtubePlayerView.alpha = 0.0
        self.stopBackgroundAnimation()
    }
    
    
    
    func handleNoSongs(){
        self.updateBackground(forSong: nil)
        self.albumCoverView.superview!.alpha = 0.0
    }
    
    func exitParty(){
        
        BluetoothManager.disconnectFromAllDevices()
    
        self.playerController.stop()
        spotifyAuthorized = false
        isSpotifyPremiumUser = false
        spotifyTracksDisabled = false
        self.playerController.appRemote?.disconnect()
        self.party = nil
        self.playerController.mediaPlayer = nil
        self.playerController.updateProgressBar(withPercent: 0.0)
        self.playerView.alpha = 0.0
        if(self.presentedViewController != nil){
            self.dismiss(animated: true, completion: nil)
        }
        self.youtubePlayerView.alpha = 0.0
        self.albumCoverView.superview!.alpha = 0.0
        self.updateBackground(forSong: self.currentSong)
        self.hidePlaylist{
            self.showPartyIfPossible(completion: { })
        }
    }
    
    
    // MARK: Actions
    
    @IBAction func hostClicked(_ sender: Any) {
         self.hideOptions {
            self.hostParty()
        }
    }
    
    @IBAction func joinClicked(_ sender: Any) {
        self.hideOptions {
            self.searchForParties()
        }
    }
    
    @IBAction func tableViewLongPressed(_ recognizer: UILongPressGestureRecognizer) {
        guard self.party?.type == .hosted else {return}
        let feedback = UISelectionFeedbackGenerator()
        feedback.selectionChanged()
        
        let p = recognizer.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: p)
        
        guard let index = indexPath?.row, recognizer.state == .began else {return}
        
        
        self.playlistDataSource.longPressedIndex = index
        self.tableView.isEditing = false
        self.tableView.isEditing = true
        self.tableView.indexPathsForVisibleRows?.forEach{
            self.tableView.cellForRow(at: $0)?.showsReorderControl = $0.row == index
        }
    }
    
    
    @IBAction func progressBarTouched(_ recognizer: UILongPressGestureRecognizer) {
        guard self.party?.type == .hosted else {return}
        if(self.party != nil && self.currentSong != nil){
            let point = recognizer.location(in: self.progressBar?.superview)
            
            self.playerController.rewind(toPercent: Double(point.x)/Double(self.view.frame.size.width))
        }
    }
    
    @objc func playClicked(_ sender: UIButton) {
        guard let songs = self.party?.songs, songs.count>0 else{return}
        
        if(sender.isSelected){
            self.playerController.pause()
        }
        else{
            self.playerController.play()
        }
        
    }
    
    @objc func backwardClicked(_ sender: Any) {
        guard let songs = self.party?.songs, songs.count>0 else {return}
        print("backward")
        self.playerController.previousSong()
    }
    
    @objc func forwardClicked(_ sender: Any) {
        guard let songs = self.party?.songs, songs.count>0 else {return}
        self.playerController.nextSong()
        print("forward")
    }
    
    @objc func moreClicked(_ sender: Any) {
        guard let _ = self.party else {return}
        self.presentMoreOptions()
        print("more")
    }
    
    @objc func addClicked(_ sender: Any) {
        guard let _ = self.party else {return}
        print("add")
        self.showSourcePicker()
    }
    
    @objc func stateClicked(_ sender: UIButton){
        guard self.party != nil else {return}
        sender.isSelected.toggle()
    }
    
    func addClicked(withType type: SongSource){
        switch type{
        case .local(link:_): self.browseMusicApp()
        case .youtube(videoId: _): self.browseYoutube()
        case .spotify(link: _): self.browseSpotify()
        case _ : self.browseMusicApp()
        }
    }
  
    
    func showSourcePicker(){
        var options: [SongSource] = [.spotify(link: ""), .youtube(videoId: "")]
        if(self.party?.type == .hosted){
            options.append(.local(link: ""))
        }
        let picker = UIAlertController(withSources: options, delegate: self.songPickersDelegate)
        self.present(picker, animated: true, completion: nil)
    }
   
    
    func updateTableAfterAddingSongs(firstSongs first: Bool){
        self.tableView.reloadData()
        guard self.party != nil else { self.tableView.selectRow(at: IndexPath(row:self.party!.songIndex, section: 0), animated: true, scrollPosition:.none); return}
        self.albumCoverView.superview!.alpha = 1.0
        if(first){
            self.playerController.handleFirstSongsAdded()
        }
        else{
            self.tableView.selectRow(at: IndexPath(row:self.party!.songIndex, section: 0), animated: true, scrollPosition:.none)
        }
        if(self.party?.type == .hosted){
            BluetoothManager.updatePartyOnRemoteDevices(self.party!)
        }
    }
    
    func updatePlaylistWithSongsData(_ data: Data, fromPeer remotePeer: BKRemotePeer){
        
        do{
            let songsDataArray : [Data] = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! [Data]
            
            songsDataArray.forEach{
                let song = songFromData($0)
                if(song != nil){
                    self.party!.songs!.append(song!)
                }
            }
            self.updateTableAfterAddingSongs(firstSongs: false)
            BluetoothManager.sendParty(party!, toDevice: remotePeer)
        }
        catch{
            return
        }
        
    }
    
    // MARK: Music adding methods:
    
    func browseMusicApp(){
        let mediaPicker = MPMediaPickerController(mediaTypes: .music)
        mediaPicker.allowsPickingMultipleItems = true
        
        mediaPicker.prompt = "Add to playlist"
        mediaPicker.showsCloudItems = false
        mediaPicker.modalPresentationStyle = UIModalPresentationStyle.formSheet
        mediaPicker.delegate = self.songPickersDelegate
        self.songPickersDelegate.mediaPicker = mediaPicker
        self.present(mediaPicker, animated: true, completion: {})
    }
    
    func addMusicAppSong(withItem item: MPMediaItem){
        
        self.party?.songs?.append(item.makeSong())
    }
    
    func browseYoutube(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "YoutubePickerViewController") as! YoutubePickerViewController
        viewController.delegate = self.songPickersDelegate
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addYoutubeSong(withInfo info: Dictionary<String,Any>){
        guard let song = info.songWithYoutubeDictionary() else {return}
        
        if(self.party!.type! == .hosted){
    
            let firstSongsAdded = self.party?.songs?.count ?? 0 == 0
            self.party?.songs?.append(song)
            self.updateTableAfterAddingSongs(firstSongs: firstSongsAdded)
        }
        else{
            BluetoothManager.sendSongs([song])
        }
    }
    
    
    
    func browseSpotify(){

        if(AppDelegate.shared!.spotifySearchManager.hasToken){
            self.showSpotifyPicker()
        }
        else{
            let manager = AppDelegate.shared!.spotifySearchManager
            manager.authorize()
        }
    }
    
    func addSpotifyTrack(_ track: SpotifyTrack){
        let song = Song(name: track.name, artist: track.artist.name, duration: 0, source: .spotify(link: track.uri), albumCover: nil)
        if(self.party!.type! == .hosted){
            let firstSongsAdded = self.party?.songs?.count ?? 0 == 0
            self.party?.songs?.append(song)
            self.updateTableAfterAddingSongs(firstSongs: firstSongsAdded)
        }
        else{
            BluetoothManager.sendSongs([song])
        }
    }
    
    func showSpotifyPicker(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SpotifyPickerViewController") as! SpotifyPickerViewController
        viewController.delegate = self.songPickersDelegate
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    
    
    
    func presentConnectionRequestSheet(forPeer peer: BKRemotePeer , withDict dict: Dictionary <String, Any>){
        if(self.presentedViewController == nil){
            let title = "Connection request"
            let message = "\(dict["nickname"]!) wants to join your party!"
            let alertController = ConnectionRequestController(title: "", message: ""
                , preferredStyle: .actionSheet)
            
            alertController.setValue(NSAttributedString(string: title, attributes: [.font : UIFont(name: "HelveticaNeue-Bold", size: 16) as Any, .foregroundColor : UIColor.white as Any]), forKey: "attributedTitle")
            alertController.setValue(NSAttributedString(string: message, attributes: [.font : UIFont(name: "HelveticaNeue", size: 15) as Any, .foregroundColor : UIColor.lightGray as Any]), forKey: "attributedMessage")
            
            self.configureAlertController(withDict: dict, alertController: alertController, remotePeer: peer)
            
            
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            self.connectionRequests.append(["peer" : peer, "dict" : dict])
        }
    }
    
    func presentMoreOptions(){
        let title = "Party options"
        let message = "What would you like to do?"
        let alertController = UIAlertController(title: "", message: ""
            , preferredStyle: .actionSheet)
        alertController.view.tintColor = .myGreen
        alertController.setValue(NSAttributedString(string: title, attributes: [.font : UIFont(name: "HelveticaNeue-Bold", size: 16) as Any, .foregroundColor : UIColor.white as Any]), forKey: "attributedTitle")
        alertController.setValue(NSAttributedString(string: message, attributes: [.font : UIFont(name: "HelveticaNeue", size: 15) as Any, .foregroundColor : UIColor.lightGray as Any]), forKey: "attributedMessage")
        if(self.party!.type! == .hosted && self.party!.connectedUsers!.keys.count > 0){
            alertController.addAction(UIAlertAction(title: "View connected users", style: .default, handler: { (_) in
                self.showUsers()
            }))
        }
        
        if(self.party!.type! == .hosted && self.party!.songs!.count > 0){
            alertController.addAction(UIAlertAction(title: "Save party", style: .default, handler: { (_) in
                self.saveParty()
            }))
        }
        
        alertController.addAction(UIAlertAction(title: self.party!.type! == .remote ? "Leave the party" : "Stop the party", style: .default, handler: { (_) in
            self.exitParty()
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in }))
        
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func saveParty(){
        var savedParties = UserDefaults.standard.object(forKey:"savedParties") as? [String:Data] ?? [String:Data]()
        guard let data = dataFromParty(self.party) else {return}
        savedParties[self.party!.name!] = data
        UserDefaults.standard.set(savedParties, forKey: "savedParties")
        UserDefaults.standard.synchronize()
    }
    
    func showUsers(){
        let title = "Connected users"
        let message = "Click on a user to kick him from the party"
        let alertController = UIAlertController(title: "", message: ""
            , preferredStyle: .actionSheet)
        alertController.view.tintColor = .myGreen
        alertController.setValue(NSAttributedString(string: title, attributes: [.font : UIFont(name: "HelveticaNeue-Bold", size: 16) as Any, .foregroundColor : UIColor.white as Any]), forKey: "attributedTitle")
        alertController.setValue(NSAttributedString(string: message, attributes: [.font : UIFont(name: "HelveticaNeue", size: 15) as Any, .foregroundColor : UIColor.lightGray as Any]), forKey: "attributedMessage")
        
        
        for (id, user) in self.party!.connectedUsers!{
            let action = UIAlertAction(title: user.nickname, style: .default, handler: { (_) in
                self.kickUser(user)
            })
            
            let image = UIImage.resizeImage(image: user.profileImage!, targetSize: CGSize(width: 40.0, height: 40.0)).roundedImage().withRenderingMode(.alwaysOriginal)
            
            
            action.setValue(image, forKey: "image")
            
            alertController.addAction(action)
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func kickUser(_ user: User){
        BluetoothManager.disconnectFromUser(user)
    }
    
    func configureAlertController(withDict dict: Dictionary <String, Any>, alertController: ConnectionRequestController, remotePeer: BKRemotePeer){
        let customView = UIView()
        alertController.peer = remotePeer
        alertController.view.addSubview(customView)
        customView.translatesAutoresizingMaskIntoConstraints = false
        customView.topAnchor.constraint(equalTo: alertController.view.topAnchor, constant: 85).isActive = true
        customView.rightAnchor.constraint(equalTo: alertController.view.rightAnchor, constant: -10).isActive = true
        customView.leftAnchor.constraint(equalTo: alertController.view.leftAnchor, constant: 10).isActive = true
        customView.heightAnchor.constraint(equalToConstant: 105).isActive = true
        
        
        let imageString = dict["profileImage"] as! String
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)
        let profileImage = imageData != nil ? UIImage(data: imageData!) : nil

        let imageView = UIImageView(image: profileImage)
        customView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        imageView.centerXAnchor.constraint(equalTo: customView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: customView.centerYAnchor).isActive = true
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 40
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.myGreen.cgColor
        alertController.view.translatesAutoresizingMaskIntoConstraints = false
        customView.bottomAnchor.constraint(equalTo: alertController.view.bottomAnchor, constant:-140).isActive = true
        let firstSubview = alertController.view.subviews.first

        let alertContentView = firstSubview!.subviews.first
        for subSubView in alertContentView!.subviews { //This is main catch
            subSubView.backgroundColor = UIColor(white:1.0, alpha: 0.05) //Here you change background
        }
        
        
        alertController.view.tintColor = .myGreen
        
//        alertController.view.heightAnchor.constraint(equalToConstant: 350).isActive = true
        
        customView.backgroundColor = .clear
        
        let okAction = UIAlertAction(title: "Accept", style: .default) { (action) in
            self.checkPendingRequests()
            print("selection")
            var userInfo = dict
            userInfo["profileImage"] = profileImage
            self.grantAccessForDevice(remotePeer, associatedUserInfo: userInfo)
        }
        
        let cancelAction = UIAlertAction(title: "Deny", style: .default) {(action) in
            self.checkPendingRequests()
            BluetoothManager.denyAccess(forDevice: remotePeer)
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
    }
    
    func checkPendingRequests(){
        if(self.connectionRequests.count > 0){
            let request = self.connectionRequests.first
            
            let peer = request!["peer"] as! BKRemotePeer
            let dict = request!["dict"] as! Dictionary<String, Any>
            self.presentConnectionRequestSheet(forPeer: peer, withDict: dict)
            self.connectionRequests = Array(self.connectionRequests.dropFirst())
        }
    }
    
    func grantAccessForDevice(_ device: BKRemotePeer, associatedUserInfo userInfo: Dictionary<String, Any>){
        var user =  User(nickname: userInfo["nickname"] as? String, profileImage: userInfo["profileImage"] as? UIImage)
        user.id = device.identifier.uuidString
        self.party!.connectedUsers![user.id!] = user
        
        BluetoothManager.sendParty(self.party!, toDevice: device)
    }
        
        // MARK: StoreKit methods:
        
    func showSpotifyAppStoreInstall() {
        if TARGET_OS_SIMULATOR != 0 {
            self.presentAlert(title: "Simulator In Use", message: "The App Store is not available in the iOS simulator, please test this feature on a physical device.", nil)
        } else {
            let loadingView = UIActivityIndicatorView(frame: view.bounds)
            view.addSubview(loadingView)
            loadingView.startAnimating()
            loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            let storeProductViewController = SKStoreProductViewController()
            storeProductViewController.delegate = self
            storeProductViewController.loadProduct(withParameters: [SKStoreProductParameterITunesItemIdentifier: SPTAppRemote.spotifyItunesItemIdentifier()], completionBlock: { (success, error) in
                loadingView.removeFromSuperview()
                if let error = error {
                    self.presentAlert(
                        title: "Error accessing App Store",
                        message: error.localizedDescription, nil)
                } else {
                    self.present(storeProductViewController, animated: true, completion: nil)
                }
            })
        }
    }
    
    public func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}

class ConnectionRequestController : UIAlertController{
    var peer: BKRemotePeer?
}

    


