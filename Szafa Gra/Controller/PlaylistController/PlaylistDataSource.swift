//
//  PlaylistDataSource.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 17/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit


class PlaylistDataSource: NSObject, UITableViewDataSource {

    var longPressedIndex: Int?
    var party: Party? {
        get{
            return PlaylistViewController.shared.party
        }
        set{
            PlaylistViewController.shared.party = newValue
        }
    }
    
    
    // MARK: UITableViewDataSource methods:
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.party?.songs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as UITableViewCell
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let movedSong = self.party!.songs![sourceIndexPath.row]
        self.party!.songs!.remove(at: sourceIndexPath.row)
        self.party!.songs!.insert(movedSong, at: destinationIndexPath.row)
        if(sourceIndexPath.row == self.party!.songIndex)
        {
            self.party!.songIndex = destinationIndexPath.row
        }
        else if(sourceIndexPath.row < self.party!.songIndex && destinationIndexPath.row > self.party!.songIndex)
        {
            self.party!.songIndex -= 1
        }
        else if(sourceIndexPath.row > self.party!.songIndex && destinationIndexPath.row <= self.party!.songIndex){
            self.party!.songIndex += 1
        }
        self.longPressedIndex = nil
        tableView.isEditing = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            tableView.selectRow(at: IndexPath(row: self.party!.songIndex, section: 0), animated: true, scrollPosition: .none)
        }
        
        BluetoothManager.updatePartyOnRemoteDevices(self.party!)
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete)
        {
            PlaylistViewController.shared.playerController.removeSong(atIndex: indexPath.row)
        }
    }
    
}
