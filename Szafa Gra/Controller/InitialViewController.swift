//
//  InitialViewController.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 29/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        self.populateWithRects()
        DispatchQueue.main.asyncAfter(deadline: .now()+2.5, execute: {
                            self.performSegue(withIdentifier: "LoadedView", sender: self)
                        })
        
//        UIView.animate(withDuration: 1.0, delay: 1.0, options: .curveEaseInOut, animations: {
//            self.view.layoutIfNeeded()
//        }) { (completed) in
//
//        }
        
    }
    
    func populateWithRects(){
        let rectWidth = CGFloat(80.0)
        let gap = CGFloat(10.0)
        let viewHeight = self.view.frame.size.height
        let viewWidth = self.view.frame.size.width
        let halfHeight = 0.5*(viewHeight-rectWidth)-gap
        let halfCountY = ceil(halfHeight/(rectWidth+gap))
        let originY = halfHeight-halfCountY*(rectWidth+gap)
        
        let halfWidth = 0.5*(viewWidth-rectWidth)-gap
        let halfCountX = ceil(halfWidth/(rectWidth+gap))
        let originX = halfWidth-halfCountX*(rectWidth+gap)
        
        for i in 0...(Int(halfCountX)*2){
            for j in 0...(Int(halfCountY)*2){
                let view = UIView(frame: CGRect(x: gap+originX+CGFloat(i)*(rectWidth+gap), y: gap+originY+CGFloat(j)*(rectWidth+gap), width: rectWidth, height: rectWidth))
                if(view.center != CGPoint(x: viewWidth*0.5, y: viewHeight*0.5)){
                    view.backgroundColor = .myDarkGreen
                    view.alpha = 0.0
                    self.view.addSubview(view)
                }
                else{
                    let imageView = UIImageView(frame: view.frame)
                    imageView.image = UIImage(named: "logo")
                    imageView.alpha = 0.0
                    self.view.addSubview(imageView)

                }
            }
        }
        self.startLoadingAnimation()
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            self.view.subviews.forEach{
                $0.alpha = 1.0
            }
        }) { (_) in
            
        }
        
        
    }
    
    func startLoadingAnimation(){
        var t: Double = 0.0
        Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true, block: { (timer) in
            t += 0.08
            self.view.subviews.forEach{
                let distance = sqrt(pow(self.view.frame.midX-$0.frame.midX,2) + pow(self.view.frame.midY-$0.frame.midY,2))
                if(distance != 0.0){
                        let angle = Double.pi * Double(distance/self.view.frame.midX)
                        let factor = CGFloat(angle)*0.015+(0.04*CGFloat(sin(angle+t))+0.96)
                        let width = factor*80.0
                    
                        let center = $0.center
                    
                        $0.frame = CGRect(x: center.x-0.5*width,y: center.y-0.5*width, width: width, height: width)
                }
                
                
            }
        })
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
