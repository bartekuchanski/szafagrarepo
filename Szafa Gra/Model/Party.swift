//
//  Party.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 29/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

struct Party {
    var name: String?
    var type: PartyType!
    var songs: [Song]?
    var songIndex = 0
    var connectedUsers : [String : User]?
    
}

enum PartyType {
    case hosted
    case remote
    case saved
    
    public var string: String {
        switch self
        {
        case .hosted : return "hosted"
        case .remote : return "remote"
        case .saved : return "saved"
        }
    }
    
}
