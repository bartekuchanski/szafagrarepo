//
//  User.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 15/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

struct User{
    
    static var current: User? = User.saved() ?? User() {
        didSet{
            if(User.current != nil){
                self.save()
            }
        }
    }
    private var _nickname : String?
    
    var nickname: String?{
        get{
            if(self._nickname == nil)
            {
                return "Anonymous"
            }
            else{
                return self._nickname
            }
        }
        set{
            self._nickname = newValue
        }
    }
    
    private var _profileImage : UIImage?
    
    var profileImage: UIImage?{
        get{
            if(self._profileImage == nil)
            {
                return UIImage(named: "profilePicture")
            }
            else{
                return self._profileImage
            }
        }
        set{
            self._profileImage = newValue
           
        }
    }
    
    var id: String?
    
    init(){}
    
    init(nickname: String?, profileImage: UIImage?){
        self.nickname = nickname
        self.profileImage = profileImage
    }
    
    static func saved() -> User?{
        var nickname: String?
        var profileImage: UIImage?
        
        nickname = UserDefaults.standard.object(forKey: "nickname") as? String
        if let data: Data = UserDefaults.standard.object(forKey: "profileImage") as? Data {
            profileImage = UIImage(data: data)
        }
        
        if(nickname != nil || profileImage != nil){
            return User(nickname: nickname, profileImage: profileImage)
        }
        else{
            return nil
        }
     
    }
    
    
    
    static func save(){
        let user = User.current
        var shouldSave = false
        if(user?._nickname != nil){
            shouldSave = true
            UserDefaults.standard.setValue(user!.nickname, forKey: "nickname")
        }
        if(user?._profileImage != nil){
            shouldSave = true
            UserDefaults.standard.setValue(user!.profileImage!.jpegData(compressionQuality: 1.0), forKey: "profileImage")
        }
        if(shouldSave){
            UserDefaults.standard.synchronize()
        }
    }
    
    
    
    
    
}
