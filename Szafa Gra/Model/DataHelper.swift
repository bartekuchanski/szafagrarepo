//
//  DataHelper.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 01/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import AVFoundation

public func convertToString(duration: Int?) -> String {
    guard let time: Int = duration else{
        return ""
    }
    let minutes = Int(time / 60)
    let seconds = Int(time % 60)
    var timeStrings: [String] = []
    [minutes, seconds].forEach{
        if($0 >= 10){
            timeStrings.append(String($0))
        }
        else{
            timeStrings.append("0"+String($0))
        }
    }
    return (timeStrings[0] + ":" + timeStrings[1])
}

public func urlOfCurrentlyPlayingInPlayer(_ player: AVPlayer) -> String? {
    return ((player.currentItem?.asset) as? AVURLAsset)?.url.absoluteString
}

func getYoutubeId(youtubeUrl: String) -> String? {
    let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
    let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
    let range = NSRange(location: 0, length: youtubeUrl.count)
    guard let result = regex?.firstMatch(in: youtubeUrl, range: range) else {
        return nil
    }
    return (youtubeUrl as NSString).substring(with: result.range)
}

func getInfoForVideo(withId id: String?, completion completionBlock:@escaping (_ dict: Dictionary<String, Any>) -> (), error errorBlock: @escaping(_ error: YoutubeError)->()){
    guard let id = id else {
        errorBlock(.wrongURL)
        return
    }
    var request = URLRequest(url: URL(string: "http://youtube.com/get_video_info?video_id=" + id)!)
    request.httpMethod = "GET"
    
    let queue = OperationQueue()
    
    NSURLConnection.sendAsynchronousRequest(request, queue: queue) { (response: URLResponse?, data: Data?, error: Error?) in
        
        if(data != nil && data!.count > 0){
            guard let objects = dictionaryByParsingData(data: data), var title = objects["title"] as! String?, let seconds = objects["length_seconds"] as! String? else { errorBlock(.wrongURL); return}
            var artist = "Other artist"
            
            if(title.contains("-")){
                let components = title.components(separatedBy: "-")
                if(components.count >= 2){
                    artist = components[0]
                    title = Array(components.dropFirst()).joined(separator: "-")
                }
            }
            let duration = Int(seconds)!
            
            let thumbnailURL = objects["thumbnail_url"] as! String?
            print("Artysta: " + artist + " Tytuł: " + title + " Czas: " + String(duration))
            let info = ["title" : title, "artist" : artist, "duration" : duration, "thumbnailURL" : thumbnailURL as Any, "videoId" : id] as Dictionary<String,Any>
            
            completionBlock(info)
        }
        else{
            errorBlock(.noConnection)
        }
    }
}

func dataFromParty(_ party: Party?) -> Data? {
    guard let party = party else {return nil}
    var songs: [[String:Any]] = []
    if(party.songs != nil){
        for song in party.songs!{
            songs.append(dictFromSong(song)!)
        }
    }
    let dict : [String : Any] = ["name" : party.name!,
                                 "songIndex" : party.songIndex,
                                 "songs" : songs,
                                 "type" : party.type.string]
    do{
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        return jsonData
    }
    catch {
        return nil
    }
    
}

func partyFromData(_ data: Data?) -> Party? {
    guard let data = data else {return nil}
    do{
        let dict : [String : Any] = try JSONSerialization.jsonObject(with: data, options: []) as! [String : Any]
        guard dict["songs"] != nil else {return nil}
        let songDicts : [[String : Any]] = dict["songs"] as! [[String : Any]]
        var songs : [Song] = []
        for songDict in songDicts{
            songs.append(songFromDict(songDict)!)
        }
        
        return Party(name: dict["name"] as? String, type: PartyType.remote, songs: songs, songIndex: dict["songIndex"] as! Int, connectedUsers: nil)
    }
    catch{
        return nil
    }
}


func dictFromSong(_ song: Song?) -> Dictionary<String,Any>? {
    guard let song = song else {return nil}
    let dict : [String : Any] = ["name" : song.name!,
                                 "artist" : song.artist!,
                                 "duration" : song.duration as Any,
                                 "sourceTag" : song.source!.buttonTag,
                                 "link" : song.source!.link]
                        
    return dict
}

func dataFromDict(_ dict: Dictionary<String,Any>?) -> Data?{
    guard let dict = dict else {return nil}
    do{
        let jsonData = try JSONSerialization.data(withJSONObject: dict as Any, options: .prettyPrinted)
        return jsonData
    }
    catch {
        return nil
    }
}

func dataFromSong(_ song: Song?) -> Data? {
    return dataFromDict(dictFromSong(song))
}

func songFromDict(_ dict: Dictionary<String, Any>?) -> Song? {
    guard let dict = dict, dict["artist"] != nil else {return nil}
    
    return Song(name: dict["name"] as? String, artist: dict["artist"] as? String, duration: dict["duration"] as? Int, source: SongSource.typeFromSource(SongSource.caseFromButtonTag((dict["sourceTag"] as! Int)), link: (dict["link"] as! String)), albumCover: nil)
   
}

func songFromData(_ data: Data?) -> Song?{
    guard let dict = dictFromData(data) else {return nil}
    return songFromDict(dict)
}


func dictFromData(_ data: Data?) -> Dictionary<String, Any>?{
    guard let data = data else {return nil}
    
    do{
        let dict : [String : Any] = try JSONSerialization.jsonObject(with: data, options: []) as! [String : Any]
        return dict
    }
    catch{
        return nil
    }
}





private func dictionaryByParsingData(data:Data!) -> Dictionary<String, Any>? {
    guard let string = String(data: data, encoding: .utf8) else {return nil}
    let keyValues = string.components(separatedBy: "&")
    var keys: [String] = []
    var values: [String] = []
    
    keyValues.forEach{
        let components = $0.components(separatedBy: "=")
        if(components.count == 2){
            
            let key = components[0].replacingOccurrences(of: "+", with: " ").removingPercentEncoding
            
            let value = components[1].replacingOccurrences(of: "+", with: " ").removingPercentEncoding
            keys.append(key!)
            values.append(value!)
        }
    }
    guard (keys.containsSameElements(as: Array(Set(keys)))) else {return nil}
    let dict = Dictionary(uniqueKeysWithValues: zip(keys, values))
    guard let _ = dict["title"] as String? else { return nil }
    
    return dict
    
}

