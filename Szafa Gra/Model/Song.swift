//
//  Song.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 31/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import MediaPlayer

public enum SongSource: Equatable {
    
    case youtube(videoId: String?)
    case spotify(link: String?)
    case local(link: String?)
    case streaming(link: String?)
    case appleMusic(link: String?)
    
    public var iconName: String {
        switch self
        {
            case .youtube(videoId: _) : return "youtubeIcon"
            case .spotify(link: _) : return "spotifyIcon"
            case .local(link: _) : return "localIcon"
            case .streaming(link: _) : return "join"
            case .appleMusic(link: _) : return "appleMusicIcon"
        }
    }
    
    public var buttonTitle: String {
        switch self
        {
            case .youtube(videoId: _) : return "YouTube"
            case .spotify(link: _) : return "Spotify"
            case .local(link: _) : return "Local library"
            case .streaming(link: _) : return "Local library"
            case .appleMusic(link: _) : return "Apple Music"
        }
    }
    
   public var link: String {
        switch self
        {
        case let .youtube(videoId: link) : return link!
        case let .spotify(link: link) : return link!
        case let .local(link: link) : return link!
        case let .streaming(link: link) : return link!
        case let .appleMusic(link: link) : return link!
        }
    }
    
    public var buttonTag: Int {
        switch self
        {
            case .youtube(videoId: _) : return 0
            case .spotify(link: _) : return 1
            case .local(link: _) : return 2
            case .streaming(link: _) : return 3
            case .appleMusic(link: _) : return 4
        }
    }
    
    public static func typeFromSource(_ songSource: SongSource, link: String) -> SongSource{
        switch songSource
        {
        case .youtube(videoId: _) : return .youtube(videoId: link)
        case .spotify(link: _) : return .spotify(link: link)
        case .local(link: _) : return .local(link: link)
        case .streaming(link: _) : return .streaming(link: link)
        case .appleMusic(link: _) : return .appleMusic(link: link)
        }
    }
    
    public static func caseFromButtonTag(_ tag: Int) -> SongSource {
        switch tag
        {
            case 0 : return .youtube(videoId: "")
            case 1 : return .spotify(link: "")
            case 2 : return .local(link: "")
            case 3 : return .streaming(link: "")
            case 4 : return .appleMusic(link: "")
            case _: return .local(link: "")
        }
    }
    public var isSpotify: Bool{
        if case .spotify(link:_) = self{
            return true
        }
        else
        {
            return false
        }
    }
}

struct Song {
    
    var name: String?
    var artist: String?
    var duration: Int?
    var source: SongSource?
    var albumCover: UIImage?
}

