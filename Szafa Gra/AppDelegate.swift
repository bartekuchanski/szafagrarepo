//
//  AppDelegate.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 28/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import SpotifyKit

var spotifyAuthorized = false
var isSpotifyPremiumUser = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SPTAppRemoteDelegate {
    
    
    
    private let redirectUri = URL(string:"comspotifytestsdk://")!
    private let clientIdentifier = "089d841ccc194c10a77afad9e1c11d54"
    private let name = "Now Playing View"
    
    // keys
    static private let kAccessTokenKey = "access-token-key"
    
    
    
    
    var accessToken = UserDefaults.standard.string(forKey: kAccessTokenKey) {
        didSet {
            let defaults = UserDefaults.standard
            defaults.set(accessToken, forKey: AppDelegate.kAccessTokenKey)
            defaults.synchronize()
        }
    }
    
    static var shared : AppDelegate?
    var window: UIWindow?
    let spotifyClientID = "6ec88edb0b2b4c09bcf7559c8d894c85"
    let spotifyRedirectURL = URL(string: "spotify-ios-quick-start://spotify-login-callback")!
    let spotifySecret = "ed190d477dc448a79e9e7fa9a354acd8"
    
    
    lazy var spotifySearchManager = SpotifyManager(with:
        SpotifyManager.SpotifyDeveloperApplication(
            clientId:     self.spotifyClientID,
            clientSecret: self.spotifySecret,
            redirectUri:  self.spotifyRedirectURL.absoluteString
        )
    )
    
    
    lazy var appRemote: SPTAppRemote = {
        let configuration = SPTConfiguration(clientID: self.spotifyClientID, redirectURL: self.spotifyRedirectURL)
        let appRemote = SPTAppRemote(configuration: configuration, logLevel: .none)
        appRemote.connectionParameters.accessToken = self.accessToken
        appRemote.delegate = self
        return appRemote
    }()
   
    
    
    
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        self.spotifySearchManager.saveToken(from: url)
        return true
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let parameters = appRemote.authorizationParameters(from: url);
        
        if let access_token = parameters?[SPTAppRemoteAccessTokenKey] {
            appRemote.connectionParameters.accessToken = access_token
            
            self.accessToken = access_token
            self.spotifySearchManager.saveToken(from: access_token)
        } else if let error_description = parameters?[SPTAppRemoteErrorDescriptionKey] {
            print("error!")
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
       // appRemote.disconnect()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppDelegate.shared = self
        UIApplication.shared.beginBackgroundTask(withName: "avplayer") {
            
        }
        //UserDefaults.standard.removeObject(forKey: "savedParties")
        self.connect()
        
    }
    
    func connect() {
        appRemote.delegate = self
        appRemote.connect()
        
    }
    
    

    

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: Spotify methods:
    

    

    func appRemoteDidEstablishConnection(_ appRemote: SPTAppRemote) {
        self.appRemote = appRemote
        self.appRemote.playerAPI?.delegate = PlaylistViewController.shared?.spotifyDelegate
        self.appRemote.playerAPI?.subscribe(toPlayerState: { (result, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }
            else{
                self.appRemote.userAPI?.delegate = PlaylistViewController.shared?.spotifyDelegate
                self.appRemote.userAPI?.fetchCapabilities(callback: { (capabilities, error) in
                   let cap = capabilities as! SPTAppRemoteUserCapabilities
                    isSpotifyPremiumUser = cap.canPlayOnDemand
                    print("ESTABLISHED!")
                    if(!isSpotifyPremiumUser){
                        PlaylistViewController.shared?.handleSpotifyFreeAccount()
                    }
                    else{
                        
                        PlaylistViewController.shared?.updateTableAfterAddingSongs(firstSongs: false)
                    }
                    
                })
            }
        })
        
        
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didFailConnectionAttemptWithError error: Error?) {
        
        
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didDisconnectWithError error: Error?) {
        print("didDisconnectWithError")
       
    }
    
    
}



