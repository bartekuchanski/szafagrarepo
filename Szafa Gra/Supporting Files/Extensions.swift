//
//  Extensions.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 17/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit
import MediaPlayer

extension String {
    func encodeStringAsUrlParameter(_ value: String) -> String {
        let escapedString = value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        return escapedString!
    }
}

extension Dictionary {
    
    func urlParametersRepresentation() -> String {
        // Add the necessary parameters
        var pairs = [String]()
        for (key, value) in self {
            let keyString = key as! String
            let valueString = value as! String
            let encodedKey = keyString.encodeStringAsUrlParameter(key as! String)
            let encodedValue = valueString.encodeStringAsUrlParameter(value as! String)
            let encoded = String(format: "%@=%@", encodedKey, encodedValue);
            pairs.append(encoded)
        }
        
        return pairs.joined(separator: "&")
    }
}

extension UIColor{
    public static var myGreen = UIColor.init(red: 134.0/255.0, green: 236.0/255.0, blue: 131.0/255.0, alpha: 1.0)
    public static var myDarkGreen = UIColor.init(red: 110.0/255.0, green: 193.0/255.0, blue: 107.0/255.0, alpha: 1.0)
}

public extension DispatchQueue {
    private static var _onceTracker = [String]()
    
    class func once(file: String = #file,
                    function: String = #function,
                    line: Int = #line,
                    block: () -> Void) {
        let token = "\(file):\(function):\(line)"
        once(token: token, block: block)
    }
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    class func once(token: String,
                    block: () -> Void) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        guard !_onceTracker.contains(token) else { return }
        
        _onceTracker.append(token)
        block()
    }
}

extension UIViewController {
    func setStatusBarStyle(_ style: UIStatusBarStyle) {
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            //statusBar.backgroundColor = style == .lightContent ? UIColor.black : .white
            statusBar.setValue(style == .lightContent ? UIColor.myGreen : UIColor(white: 0.2, alpha: 1), forKey: "foregroundColor")
            guard let navController = self.navigationController else{return}
            navController.navigationBar.tintColor = style == .lightContent ? UIColor.myGreen : UIColor(white: 0.2, alpha: 1)
            let textAttributes = [NSAttributedString.Key.foregroundColor:navController.navigationBar.tintColor]
            navController.navigationBar.titleTextAttributes = textAttributes
        }
    }
    
    private func displayError(_ error: NSError?) {
        if let error = error {
            presentAlert(title: "Error", message: error.description, nil)
        }
    }
    
    func presentAlert(title: String, message: String, _ completion:(()->())?) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = .myGreen
        alert.setValue(NSAttributedString(string: title, attributes: [.font : UIFont(name: "HelveticaNeue-Bold", size: 18) as Any, .foregroundColor : UIColor.white as Any]), forKey: "attributedTitle")
        alert.setValue(NSAttributedString(string: message, attributes: [.font : UIFont(name: "HelveticaNeue", size: 16) as Any, .foregroundColor : UIColor.lightGray as Any]), forKey: "attributedMessage")
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            completion?()
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        
    }
}

extension UIAlertController{
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let feedback = UINotificationFeedbackGenerator()
        feedback.notificationOccurred(.success)
    }
    
    convenience init(withSources sources: [SongSource], delegate: SourcePickerViewDelegate){
       self.init(title: "", message: "", preferredStyle: .actionSheet)
        let title = "Add songs"
        let message = "Choose source of the new songs"
        
        self.view.tintColor = .myGreen
        self.setValue(NSAttributedString(string: title, attributes: [.font : UIFont(name: "HelveticaNeue-Bold", size: 18) as Any, .foregroundColor : UIColor.white as Any]), forKey: "attributedTitle")
        self.setValue(NSAttributedString(string: message, attributes: [.font : UIFont(name: "HelveticaNeue", size: 16) as Any, .foregroundColor : UIColor.lightGray as Any]), forKey: "attributedMessage")
        for source in sources{
            let action = UIAlertAction(title: source.buttonTitle, style: .default){ action in
                delegate.sourcePickerButtonClicked(tag: source.buttonTag)
            }
            let image = UIImage.resizeImage(image: UIImage(named: source.iconName)!, targetSize: CGSize(width: 40.0, height: 40.0)).roundedImage().withRenderingMode(.alwaysOriginal)
            
            action.setValue(image, forKey: "image")
            self.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            
        }
        self.addAction(cancelAction)
    }
    
    @objc func hide(){
        self.dismiss(animated: true, completion: nil)
    }
}


extension MPMediaItem {
    
    func makeSong() -> Song {
        return Song(name: self.title, artist: self.artist, duration: Int(self.playbackDuration), source: SongSource.local(link: self.assetURL?.absoluteString), albumCover: self.artwork?.image(at: CGSize(width: 600.0, height: 600.0)))
    }
}

extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}

extension Dictionary{
    func songWithYoutubeDictionary() -> Song?{
        guard let dict = self as? Dictionary<String,Any> else {return nil}
        return Song(name: (dict["title"] as! String), artist: (dict["artist"] as! String), duration: (dict["duration"] as! Int), source: SongSource.youtube(videoId: (dict["videoId"] as! String)), albumCover: nil)
        
    }
}

extension UIImage{
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func roundedImage() -> UIImage{
        // Begin a new image that will be the new image with the rounded corners
        // (here with the size of an UIImageView)
        UIGraphicsBeginImageContextWithOptions(self.size, false, 1.0);
        let bounds = CGRect(origin: CGPoint(x: 0, y: 0), size: self.size)
        // Add a clip before drawing anything, in the shape of an rounded rect
        UIBezierPath(roundedRect: bounds,
                     cornerRadius:self.size.width*0.5).addClip()
        // Draw your image
        self.draw(in: bounds)
        
        // Get the image, here setting the UIImageView image
        let image = UIGraphicsGetImageFromCurrentImageContext();
        
        // Lets forget about that we were drawing
        UIGraphicsEndImageContext();
        return image!
    }
}

