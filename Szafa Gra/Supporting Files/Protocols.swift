//
//  Protocols.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 17/06/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//
import SpotifyKit

@objc protocol SourcePickerViewDelegate {
    func sourcePickerButtonClicked(tag: Int)
}

protocol SpotifyPickerDelegate: AnyObject{
    func spotifyPickerDidPick(track: SpotifyTrack)
}

protocol YoutubePickerViewControllerDelegate{
    func youtubePicker(picker:YoutubePickerViewController, didPickVideoWithInfo info: Dictionary<String,Any>)
    func youtubePicker(picker:YoutubePickerViewController, didFailPickingVideoWithError error: YoutubeError)
}

protocol DataViewController{
    var pageIndex: Int { get set }
    
}
