//
//  PartyCell.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 29/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

class PartyCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    
    func setupCell(withParty party: Party, atIndex index: Int){
        self.backgroundColor = UIColor.black.withAlphaComponent(index%2==0 ? 0.1 : 0.2)
        self.nameLabel.text = party.name
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if(selected){
            self.contentView.backgroundColor = UIColor.myGreen.withAlphaComponent(0.7)
            self.nameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
            self.nameLabel.textColor = .white
            
        }
        else{
            self.nameLabel.font = UIFont(name: "HelveticaNeue-Light", size: 15)
            self.contentView.backgroundColor = .clear
            self.nameLabel.textColor = .lightGray
            
            
        }
        
        // Configure the view for the selected state
    }
}
