//
//  SongCell.swift
//  Szafa Gra
//
//  Created by Bartłomiej Uchański on 30/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import UIKit

class SongCell: UITableViewCell {
    
    @IBOutlet weak var sourceIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.sourceIcon.tintColor = .myGreen
    }

    
    func setupCell(atIndex index:Int){
        self.backgroundColor = UIColor.black.withAlphaComponent(index%2==0 ? 0.1 : 0.2)
        
    }
    
    func populate(withSong song: Song!){
        self.titleLabel.text = song!.artist! + " - " + song!.name!
        
        self.sourceIcon.image = UIImage(named: song.source?.iconName ?? "songPlaceholder")
        if case .spotify(_) = song!.source!{
           // self.isUserInteractionEnabled = !spotifyTracksDisabled
            self.backgroundColor = spotifyTracksDisabled ? .gray : .clear
        }
        else{
            //self.isUserInteractionEnabled = true
            self.backgroundColor = .clear
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if(selected){
            self.contentView.backgroundColor = UIColor.myGreen.withAlphaComponent(0.7)
            self.titleLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
            
        }
        else{
           self.titleLabel.font = UIFont(name: "HelveticaNeue-Light", size: 14)
            self.contentView.backgroundColor = .clear
            
            
        }
        
        // Configure the view for the selected state
    }
    
}
