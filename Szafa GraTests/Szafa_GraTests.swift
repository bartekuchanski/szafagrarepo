//
//  Szafa_GraTests.swift
//  Szafa GraTests
//
//  Created by Bartłomiej Uchański on 28/05/2019.
//  Copyright © 2019 Bartłomiej Uchański. All rights reserved.
//

import XCTest
@testable import Szafa_Gra

class Szafa_GraTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSongSources() {
        let link = "http://www.spotify.com"
        let x = SongSource.spotify(link: link)
        let iconName = "spotifyIcon"
        if case let .spotify(link:n) = x {
          XCTAssert(n == link)
        }
        else{
            XCTFail()
        }
        XCTAssert(iconName == x.iconName)
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
